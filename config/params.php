<?php

return [
	'siteName' => Yii::t('app', 'Мобильное управление'),
	// 'domain' => 'http://tselin.kz',
    'siteEmail' => 'robot@eregion.kz',
    'adminEmail' => 'admin@example.com',
    'error_message' => 'Ошибка. Попробуйте еще раз или обратитесь к администратору',
    'email-views' => [
    	'view1-html' => 'Шаблон от такой организации'
    ],

    'application' => [
    	'seriosness' => [
    		'1' => 'Обычная',
    		'2' => 'Средняя',
    		'3' => 'Высокая'
    	],

    	'status' => [
    		'1' => Yii::t('app', 'Проверяется'),
    		'2' => Yii::t('app', 'В работе'),
    		'0' => Yii::t('app', 'В заморозке'),
    		'3' => Yii::t('app', 'Выполнена'),
    		'-1' => Yii::t('app', 'Удалена')
    	],

    	'days_to_add' => '+1 day',

    	'files_types' => [
			'1' => 'Заявитель',
			'2' => 'Ответчик'
		]
    ],

    'master' => 'jarvis'
];
