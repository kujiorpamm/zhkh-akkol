<?php

use yii\helpers\Html;
?>
<div class="i18n-message-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
