<?php
use yii\bootstrap\ActiveForm;

?>

<div id="page-registration">

	<div class="form-container">
		<p><?=Yii::t('app', 'Укажите Ваш новый пароль:')?></p>
		<br/>

		<?php $form = ActiveForm::begin(); ?>
		<?= $form->errorSummary($model); ?>

		<?= $form->field($model, 'password')->passwordInput(['placeholder'=>$model->getAttributeLabel('password')])->label(false)?>

		<center><input class="btn btn-success" type="submit" value="<?=Yii::t('app', 'Сменить пароль')?>"/></center>

		<?php ActiveForm::end(); ?>
	</div>

</div>
