<?php
	
	app\assets\DashboardAsset::register($this);
	
	$this->registerJs("
		
		[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
				new CBPFWTabs( el );
			});
	
	", $this::POS_READY);
?>

<style>
	@font-face {
		font-family: "Exo2Regular";
		src: url("/vendor/dashboard/fonts/Exo2Regular.eot");
		src: url("/vendor/dashboard/fonts/Exo2Regular.eot?#iefix")format("embedded-opentype"),
		url("/vendor/dashboard/fonts/Exo2Regular.woff") format("woff"),
		url("/vendor/dashboard/fonts/Exo2Regular.ttf") format("truetype");
		font-style: normal;
		font-weight: normal;
	}
	
	
</style>


<div class="container">
	<h2><?=Yii::t('app','Потребление воды городом за истекшие сутки')?></h2>
</div>

	<div id="panel_bg" >
		<div id="w_container">
			<div class="w"><div class="b"></div></div>
			<div class="w1"><div class="b"></div></div>	
			<div class="w2"><div class="b"></div></div>
			<div class="w3"><div class="b"></div></div>
			<div class="w4"><div class="b"></div></div>
			<div class="w5"><div class="b"></div></div>
		</div>
					
			<div id="tab">
				<div id="row">
					<div id="cell">
					</div>	
					<div id="cell" class="title1">
						<?=Yii::t('app','Питьевая вода');?>
					</div>
					<div id="cell" class="title1">
						<?=Yii::t('app','Техническая вода');?>
					</div>
					<div id="cell" class="title1">
						<?=Yii::t('app','Горячая вода');?>
					</div>
					<div id="cell">
					</div>				
				</div>
				<div id="row">
					<div id="cell" class="c1">
						<div id="d1" style="display: none;">
							<div id="dev1"></div>
						</div>
					</div>
					<div id="cell" class="c2">
						<div id="d2">
							<div id="dev2"></div>
							<div style="font-weight: bold; position: relative; top: 150px;left: 60px;"><?=Yii::t('app','м3/сутки');?></div>
						</div>
					</div>
					<div id="cell" class="c2">
						<div id="d2">
							<div id="dev3-2"></div>
							<div style="font-weight: bold; position: relative; top: 150px;left: 60px;"><?=Yii::t('app','м3/сутки');?></div>
						</div>
					</div>
					<div id="cell" class="c2">
						<div id="d2" style="border-right: 0;">
							<div id="dev4-2"></div>
							<div style="font-weight: bold; position: relative; top: 150px;left: 60px;"><?=Yii::t('app','м3/сутки');?></div>
						</div>
					</div>
					<div id="cell" class="c3" style="display: none;">
						<div id="label"></div><div id="ts" style="display: none;"><?=Yii::t('app','м3/<br>сутки');?></div>
						<div id="vz" style="display: none;"><?=Yii::t('app','в/з');?></div><div id="nz"><?=Yii::t('app','м3/сутки');?></div>
						<div id="d3">
							<div id="row">
								<div id="cell" class="c3-col"><div id="dev3-1" style="display: none;"></div></div>
								<div id="cell" class="c3-col"><div id="dev3-2"></div></div>
								<div id="cell" class="c3-col"><div id="dev3-3" style="display: none;"></div></div>
								<img src="/vendor/dashboard/images/r.png" class="wheel" id="w1" style="display: none;"><img src="/vendor/dashboard/images/r.png" class="wheel" id="w2"><img src="/vendor/dashboard/images/r.png" class="wheel" id="w3" style="display: none;">
								<img src="/vendor/dashboard/images/a1.png" class="wheel" id="a1" style="display: none;"><img src="/vendor/dashboard/images/a2.png" class="wheel" id="a2"><img src="/vendor/dashboard/images/a3.png" class="wheel" id="a3" style="display: none;">	
							</div>
						</div>
					</div>
					<div id="cell" class="c4" style="display: none;">
						<div id="label"></div><div id="ts"></div>
						<div id="vz" style="display: none;"><?=Yii::t('app','в/з');?></div><div id="nz"><?=Yii::t('app','м3/сутки');?></div>
						<div id="d4">
							<div id="row">
								<div id="cell" class="c3-col"><div id="dev4-1" style="display: none;"></div></div>
								<div id="cell" class="c3-col" style="padding-left: 15px;"><div id="dev4-2"></div></div>
								<div id="cell" class="c3-col"><div id="dev4-3" style="display: none;"></div></div>
								<img src="/vendor/dashboard/images/r.png" class="wheel" id="w1" style="display: none;"><img src="/vendor/dashboard/images/r.png" class="wheel" id="w2" style="margin-left: -60px;"><img src="/vendor/dashboard/images/r.png" class="wheel" id="w3" style="display: none;">
								<img src="/vendor/dashboard/images/a4.png" class="wheel" id="a1" style="display: none;"><img src="/vendor/dashboard/images/a1.png" class="wheel" id="a2" style="margin-left: -60px;"><img src="/vendor/dashboard/images/a2.png" class="wheel" id="a3" style="display: none;">
							</div>
						</div>
					</div>
					<div id="cell" class="c5" style="display: none;">
						<div id="label"><?=Yii::t('app','Питьевая<br>вода');?></div><div id="ts"><?=Yii::t('app','тонн/<br>сутки');?></div>
						<div id="vz"><?=Yii::t('app','в/з');?></div><div id="nz"><?=Yii::t('app','н/з');?></div>
						<div id="d5">
							<div id="row">
								<div id="cell" class="c3-col"><div id="dev5-1"></div></div>
								<div id="cell" class="c3-col"><div id="dev5-2"></div></div>
								<div id="cell" class="c3-col"><div id="dev5-3"></div></div>
								<img src="/vendor/dashboard/images/r.png" class="wheel" id="w1"><img src="/vendor/dashboard/images/r.png" class="wheel" id="w2"><img src="/vendor/dashboard/images/r.png" class="wheel" id="w3">
								<img src="/vendor/dashboard/images/a3.png" class="wheel" id="a1"><img src="/vendor/dashboard/images/a4.png" class="wheel" id="a2"><img src="/vendor/dashboard/images/a1.png" class="wheel" id="a3">			
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="margin-top:-77px"></div>
		<div class="water">
			<div class="bubbles"></div>
		</div>
		<div id="top_panel"><div id="data"></div>
			
			<div style="width:100%;height:70px"></div>
			<div class="top_panel_glass">
			<div id="panel"  >
				<div id="tab">
					<div id="row" >
						<div id="cell" class="title2 col-1">
							<?=Yii::t('app','Направление');?>
						</div>
						<div id="cell" class="title2 col-2">
							<?=Yii::t('app','Расход сетевой воды');?>
						</div>
						<div id="cell" class="title2 col-3" style="display:none;">
							<?=Yii::t('app','Подпитка');?>
						</div>
						<div id="cell" class="title2 col-4">
							<?=Yii::t('app','Давление');?>
						</div>
						<div id="cell" class="title2 col-5" style="padding-right:30px">
							<?=Yii::t('app','Температура');?>
						</div>				
					</div>
					<div id="row">
						<div id="cell">
							<div class="object">
								<span><?=Yii::t('app','Верхняя зона города');?></span>
							</div>
						</div>
						<div id="cell"><div id="m"><?=Yii::t('app','м<sup>3</sup>/час');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev-1-1"></div>
											<div id="d-1-1"></div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev-1-2"></div>
											<div id="d-1-1"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="cell" style="display:none;" ><div id="m-h"></div>
							<div class="obj1-1">
								<div id="dev-1-3" style="display:none;"></div>
							</div>
						</div>
						<div id="cell"><div id="m" style="width: initial;"><?=Yii::t('app','кгс/см<sup>2</sup>');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev15"></div>
											  <div id="meter_o">
												<div id="meter_1"></div>
											  </div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev16"></div>
											  <div id="meter_o">
												<div id="meter_2"></div>
											  </div>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div id="cell">
							<div class="obj1-1">
								<div id="d-1-3">
									<div id="tab" class="temp">
										<div id="row">
											<div id="cell" class="tmp">
												<div id="outer-1-1">
													<div id="p-1-1" >
														<div id="t11" ></div>
													</div>
												</div>	
											</div>
											<div id="cell" class="tmp">
												<div id="outer-1-2">
													<div id="p-1-2">
														<div id="t12" ></div>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>						
					</div>
					<div id="row">
						<div id="cell">
							<div class="object">
								<span><?=Yii::t('app','Нижняя зона города');?></span>
							</div>
						</div>
						<div id="cell"><div id="m"><?=Yii::t('app','м<sup>3</sup>/час');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev-2-1"></div>
											<div id="d-1-1"></div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev-2-2"></div>
											<div id="d-1-1"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="cell" style="display:none;"><div id="m"><?=Yii::t('app','м<sup>3</sup>/час');?></div>
							<div class="obj1-1">
								<div id="dev-2-3"></div>
								<div id="d-1-1"></div>
							</div>
						</div>
						<div id="cell"><div id="m" style="width: initial;"><?=Yii::t('app','кгс/см<sup>2</sup>');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev17"></div>
											  <div id="meter_o">
												<div id="meter_3"></div>
											  </div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev18"></div>
											  <div id="meter_o">
												<div id="meter_4"></div>
											  </div>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div id="cell">
							<div class="obj1-1">
								<div id="d-1-3">
									<div id="tab" class="temp">
										<div id="row">
											<div id="cell" class="tmp">
												<div id="outer-1-1">
													<div id="p-2-1" >
														<div id="t21" ></div>
													</div>
												</div>	
											</div>
											<div id="cell" class="tmp">
												<div id="outer-1-2">
													<div id="p-2-2">
														<div id="t22" ></div>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>						
					</div>
					<div id="row">
						<div id="cell">
							<div class="object">
								<span class="line2"><?=Yii::t('app','22 микрорайон');?></span>
							</div>
						</div>
						<div id="cell"><div id="m"><?=Yii::t('app','м<sup>3</sup>/час');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev-3-1"></div>
											<div id="d-1-1"></div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev-3-2"></div>
											<div id="d-1-1"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="cell" style="display:none;"><div id="m"><?=Yii::t('app','м<sup>3</sup>/час');?></div>
							<div class="obj1-1">
								<div id="dev-3-3"></div>
								<div id="d-1-1"></div>
							</div>
						</div>
						<div id="cell"><div id="m" style="width: initial;"><?=Yii::t('app','кгс/см<sup>2</sup>');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev19"></div>
											  <div id="meter_o">
												<div id="meter_5"></div>
											  </div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev20"></div>
											  <div id="meter_o">
												<div id="meter_6"></div>
											  </div>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div id="cell">
							<div class="obj1-1">
								<div id="d-1-3">
									<div id="tab" class="temp">
										<div id="row">
											<div id="cell" class="tmp">
												<div id="outer-1-1">
													<div id="p-3-1" >
														<div id="t31" ></div>
													</div>
												</div>	
											</div>
											<div id="cell" class="tmp">
												<div id="outer-1-2">
													<div id="p-3-2">
														<div id="t32" ></div>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>						
					</div>	
					<div id="row">
						<div id="cell">
							<div class="object">
								<span><?=Yii::t('app','31 микрорайон');?></span>
							</div>
						</div>
						<div id="cell"><div id="m"><?=Yii::t('app','м<sup>3</sup>/час');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev-4-1"></div>
											<div id="d-1-1"></div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev-4-2"></div>
											<div id="d-1-1"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="cell" style="display:none;"><div id="m-h"><?=Yii::t('app','м<sup>3</sup>/час');?></div>
							<div class="obj1-1">
								<div id="dev-4-3"></div>
								<div id="d-1-1"></div>
							</div>
						</div>
						<div id="cell"><div id="m" style="width: initial;"><?=Yii::t('app','кгс/см<sup>2</sup>');?></div>
							<div class="obj1-2">
								<div id="tab" style="width:231px;height:99px;">
									<div id="row">
										<div id="cell">
											<div id="dev21"></div>
											  <div id="meter_o">
												<div id="meter_7"></div>
											  </div>
										</div>
										<div id="arrow">
										</div>
										<div id="cell">
											<div id="dev22"></div>
											  <div id="meter_o">
												<div id="meter_8"></div>
											  </div>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div id="cell">
							<div class="obj1-1">
								<div id="d-1-3">
									<div id="tab" class="temp">
										<div id="row">
											<div id="cell" class="tmp">
												<div id="outer-1-1">
													<div id="p-4-1" >
														<div id="t41" ></div>
													</div>
												</div>	
											</div>
											<div id="cell" class="tmp">
												<div id="outer-1-2">
													<div id="p-4-2">
														<div id="t42" ></div>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>						
					</div>	
				</div>
			</div> <!-- end of panel -->
			</div>
			<div class="glass1"></div>		
			<div id="title"><?=Yii::t('app','Информация о качестве питьевой воды перед поступлением в распределительную сеть г.Актау');?></div>	
		</div> <!-- end of top panel -->
		
	</div> <!-- end of panel_bg -->
	<div id="white_bg">	
	</div>

<div id="menu_container">
		
		
	<section style="    margin-top: 25px;">
		<div class="tabs tabs-style-">
			<nav>
				<ul>
					<li class="col1"><a href="#section--1" id="s1"><span><h2><?=Yii::t('app','Вещества,<br>присутствующие в воде в результате хлорирования');?></h2></span></a></li>
					<li class="col2"><a href="#section--2" id="s2"><span><h2><?=Yii::t('app','Органолептические показатели');?></h2></a></span></li>
					<li class="col3"><a href="#section--3" id="s3"><span><h2><?=Yii::t('app','Неорганические вещества');?></h2></a></span></li>
					<li class="col4"><a href="#section--4" id="s4"><span><h2><?=Yii::t('app','Обобщённые показатели');?></h2></a></span></li>
					<li class="col5"><a href="#section--5" id="s5"><span><h2><?=Yii::t('app','Микробиологические показатели');?></h2></a></span></li>
				</ul>
			</nav>
				<div class="content-wrap">
					<section id="section--1">
						<div id="menu_bg">
							<div id="tab">
								<div id="row">
									<div id="cell" class="sec1-1">
										<div id="name1"><?=Yii::t('app','Хлор остаточный');?></div>
										<div id="val-1-1"></div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-1-1"></div>
										<div id="p-bar-1-1"></div>
										<div id="low1-1">0,3</div>
										<div id="high1-1">0,5</div>
										<div id="label1-1"><?=Yii::t('app','Нормативный диапазон в зимний период');?></div>
									</div>
									<div id="cell" class="sec1-2">
										<div id="name1"><?=Yii::t('app','Хлороформ');?></div>
										<div id="m1" style="font-size: 27px;"> < </div>
										<div id="val-1-2"></div>&nbsp;&nbsp;<div id="m1">мг/л</div>
										<div id="p-bg"></div>
										<div id="p-level-1-2"></div>
										<div id="p-bar-1-2"></div>
										<div id="high1-2">0,2</div>
										<div id="label1-2"><?=Yii::t('app','Норматив не более');?></div>
										
									</div>
									<div id="cell" style="position:relative;width:690px;bottom:10px">
										<div id="label_text">
										<?=Yii::t('app','Санитарные правила, утвержденные Приказом МНЭ РК 16.03.2015 №209<br>по согласованию с РГУ &laquo;ДЭПП МО комитета по защите прав потребителей МИЭ РК&raquo;');?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section id="section--2">
						<div id="menu_bg" style="height:190px">
							<div id="tab">
								<div id="row">
									<div id="cell">
										<div id="name2"><?=Yii::t('app','Цветность');?></div>
									</div>
									<div id="cell">
										<div id="name2"><?=Yii::t('app','Мутность');?></div>
									</div>	
									<div id="cell">
										<div id="name2"><?=Yii::t('app','Запах');?></div>
									</div>		
									<div id="cell">
										<div id="name2"><?=Yii::t('app','Привкус');?></div>
									</div>										
								</div>								
								<div id="row">
									<div id="cell" class="sec2">
										<div id="val2-1"></div><div id="m2"><?=Yii::t('app','мг/л');?></div>
										<div id="r2"><div id="helper"></div>
											<div id="r-val-1-1"></div>
										</div>
										<div id="high2" style="top: 30px; margin-left: 41px;">20</div>
										<div id="label2" style="margin-left: 20px; margin-top: 35px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec2">
										<div id="val2-2"></div><div id="m2"><?=Yii::t('app','мг/л');?></div>
										<div id="r2"><div id="helper"></div>
											<div id="r-val-1-2"></div>
										</div>
										<div id="high2" style="top: 30px; margin-left: 41px;">1,5</div>
										<div id="label2" style="margin-left: 20px; margin-top: 35px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec2">
										<div id="val2-3" style="margin-left: 24px;"></div><div id="m2" style="left:40px;"><?=Yii::t('app','баллов');?></div>
										<div id="r2"><div id="helper"></div>
											<div id="r-val-1-3"></div>
										</div>
										<div id="high2" style="top: 30px; margin-left: 41px;">2</div>
										<div id="label2" style="margin-left: 20px; margin-top: 35px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec2 no-br" >
										<div id="val2-4" style="margin-left: 24px;"></div><div id="m2" style="left:40px;"><?=Yii::t('app','баллов');?></div>
										<div id="r2"><div id="helper"></div>
											<div id="r-val-1-4"></div>
										</div>
										<div id="high2" style="top: 30px; margin-left: 41px;">2</div>
										<div id="label2" style="margin-left: 20px; margin-top: 35px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section id="section--3">
						<div id="menu_bg" style="height:370px">
							<div id="h25"></div>
							<div id="tab">
								<div id="row">
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Железо');?></div><div id="label3"><?=Yii::t('app','Fe, суммарно');?></div>
										<div id="val3-1">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-3-1"></div>
										<div id="p-bar-3"></div>
										<div id="high3">0,3</div>
										<div id="label1-2" style="margin-left: 20px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Медь');?></div><div id="label3"><?=Yii::t('app','Cu, суммарно');?></div>
										<div id="val3-2">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-3-2"></div>
										<div id="p-bar-3"></div>
										<div id="high3">1</div>
										<div id="label1-2" style="margin-left: 20px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Фториды');?></div><div id="label3">F<sup>-</sup></div>
										<div id="val3-3">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-3-3"></div>
										<div id="p-bar-3"></div>
										<div id="high3">0,7</div>
										<div id="label1-2" style="margin-left: 15px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Нитрит-ион');?></div>
										<div id="val3-4">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-3-4"></div>
										<div id="p-bar-3"></div>
										<div id="high3">3</div>
										<div id="label1-2" style="margin-left: 20px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3 no-br">
										<div id="name3"><?=Yii::t('app','Аммиак');?></div><div id="label3"><?=Yii::t('app','по азоту');?></div>
										<div id="val3-5">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-3-5"></div>
										<div id="p-bar-3"></div>
										<div id="high3">2</div>
										<div id="label1-2" style="margin-left: 20px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>																
								</div>
							</div>
							<div id="h45">
							</div>
							<div id="tab" style="position:static">
								<div id="row">
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Нитраты');?></div><div id="label3"><?=Yii::t('app','по NO<sub>3</sub>');?></div>
										<div id="val-3-1">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="level-3-1"></div>
										<div id="p-bar-3"></div>
										<div id="high3">45</div>
										<div id="label1-2" style="margin-left: 17px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Хлориды');?></div><div id="label3">CL</div>
										<div id="val-3-2">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="level-3-2"></div>
										<div id="p-bar-3"></div>
										<div id="high3">350</div>
										<div id="label1-2" style="margin-left: 17px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Натрий');?></div>
										<div id="val-3-3">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="level-3-3"></div>
										<div id="p-bar-3"></div>
										<div id="high3">200</div>
										<div id="label1-2" style="margin-left: 17px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name3"><?=Yii::t('app','Сульфаты');?></div><div id="label3">SO<sub>4</sub></div>
										<div id="val-3-4">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="level-3-4"></div>
										<div id="p-bar-3"></div>
										<div id="high3">500</div>
										<div id="label1-2" style="margin-left: 17px; width: auto;bottom: 3px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3 no-br">
										<div id="name3"><?=Yii::t('app','Кальций');?></div><div id="label3">Ca<sup>2+</sup></div>
										<div id="val-3-5">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="level-3-5"></div>
										<div id="p-bar-3"></div>
										<div id="label3" style="bottom: 3px;"><?=Yii::t('app','Не нормируется');?></div>
									</div>																	
								</div>
							</div>	
							<div class="label_text">
							<?=Yii::t('app','Санитарные правила, утвержденные Приказом МНЭ РК 16.03.2015 №209');?> 
							</div>								
						</div>						
					</section>
					<section id="section--4">
						<div id="menu_bg" style="height:200px !important;">
						<div id="h25"></div>								
							<div id="tab">
								<div id="row">
									<div id="cell" class="sec3">
										<div id="name4"><?=Yii::t('app','Водородный показатель');?></div>
										<div id="val4-1">0</div>&nbsp;&nbsp;<div id="m1">рН</div>
										<div id="p-bg"></div>
										<div id="p-level-4-1"></div>
										<div id="p-bar-4-1"></div>
										<div id="low4-1">6</div>
										<div id="high4-1">9</div>
										<div id="label4-1"><?=Yii::t('app','Нормативный диапазон');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name4"><?=Yii::t('app','Общая минерализация');?></div><div id="label3"><?=Yii::t('app','(сухой остаток)');?></div>
										<div id="val4-2">0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-4-2"></div>
										<div id="p-bar-4-2"></div>
										<div id="high4-2">1000</div>
										<div id="label4-2" style="width: auto; margin-left: 75px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name4"><?=Yii::t('app','Жесткость общая');?></div>
										<div id="val4-3">0.0</div>&nbsp;&nbsp;<div id="m1"><?=Yii::t('app','мг-экв./л');?></div>
										<div id="p-bg"></div>
										<div id="p-level-4-3"></div>
										<div id="p-bar-3"></div>
										<div id="high3">7,0</div>
										<div id="label1-2" style="width: auto; margin-left: 18px;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
									<div id="cell" class="sec3">
										<div id="name4"><?=Yii::t('app','Окисляемость перманганатная');?></div>
										<div id="val4-4">0.00</div>&nbsp;&nbsp;<div id="m4"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg4"></div>
										<div id="p-level-4-4" style="bottom:-18px;left:116px;width:23px"></div>
										<div id="p-bar-4-4"></div>
										<div id="high4-4" style="margin-top: 24px;">5,0</div>
										<div id="label4-4" style="width: auto;"><?=Yii::t('app','Норматив:<br>не более');?></div>
									</div>		
									<div id="cell" class="sec3 no-br">
										<div id="name4"><?=Yii::t('app','Нефтепродукты');?></div><div id="label3"><?=Yii::t('app','суммарно');?></div>
										<div id="val4-5">0.00</div>&nbsp;&nbsp;<div id="m4"><?=Yii::t('app','мг/л');?></div>
										<div id="p-bg4"></div>
										<div id="p-level-4-5"></div>
										<div id="p-bar-4-4"></div>
										<div id="high4-4" style="margin-top: 46px;">0,1</div>
										<div id="label4-4"  style="width: auto; margin-top:16px;"><?=Yii::t('app','Норматив:<br>не более');?></div>
									</div>											
								</div>
							</div>
							<div class="label_text" style="bottom: 5px; top:initial">
							<?=Yii::t('app','Санитарные правила, утвержденные Приказом МНЭ РК 16.03.2015 №209');?> 
							</div>
						</div>						
					</section>
					<section id="section--5">
						<div id="menu_bg">
							<div id="h25">
							</div>								
							<div id="tab">
								<div id="row">
									<div id="cell" class="sec5">
										<div id="name5"><?=Yii::t('app','Общие колиформные<br>бактерии');?></div>
										<div id="high5" style="top: 25px;"><?=Yii::t('app','Отсутствие');?></div>
										<div id="label5" style="top: 58px;"><?=Yii::t('app','Норматив');?></div>
										<div id="val-5-1">Отсутствуют</div>
										<div id="m5-1"><?=Yii::t('app','Число бактерий в 100 мл');?></div>
									</div>
									<div id="cell" class="sec5">
										<div id="name5"><?=Yii::t('app','Термотолерантные <br>колиформные бактерии');?></div>
										<div id="high5" style="top: 25px;"><?=Yii::t('app','Отсутствие');?></div>
										<div id="label5" style="top: 58px;"><?=Yii::t('app','Норматив');?></div>
										<div id="val-5-2">Отсутствуют</div>
										<div id="m5-2"><?=Yii::t('app','Число бактерий в 100 мл');?></div>
										
									</div>
									<div id="cell" class="sec5 no-br">
										<div id="name5"><?=Yii::t('app','Общее<br>микробное число');?></div>
										<div id="val-5-3">0</div><div id="m5-3"><?=Yii::t('app','');?></div>
										<div id="p-bg5"></div>
										<div id="level-5-3"></div>
										<div id="p-bar-5"></div>
										<div id="high5-3">50</div>
										<div id="label5-3" style="top: 105px; left: 100px;width: initial;"><?=Yii::t('app','Норматив: не более');?></div>
									</div>
								</div>
							<div class="label_text" style="bottom: 5px; top:initial">
							<?=Yii::t('app','Санитарные правила, утвержденные Приказом МНЭ РК 16.03.2015 №209');?> 
							</div>
							</div>	
							<div id="h25"></div>									
						</div>						
					</section>
				</div><!-- /content -->
		</div><!-- /tabs -->
	</section>
</div>

<form>
	<input type="hidden" name="curr_date" id="curr_date" value="<?=$model->date_human;?>">
	<input type="hidden" name="v-1-1" id="v-1-1" value="<?=$model->v1;?>">
	<input type="hidden" name="v-1-2" id="v-1-2" value="<?=$model->v2;?>">
	<input type="hidden" name="v-2-1" id="v-2-1" value="<?=$model->v3;?>"> 
	<input type="hidden" name="v-2-2" id="v-2-2" value="<?=$model->v4;?>">
	<input type="hidden" name="v-2-3" id="v-2-3" value="<?=$model->v5;?>">
	<input type="hidden" name="v-2-4" id="v-2-4" value="<?=$model->v6;?>">
	<input type="hidden" name="v-3-1" id="v-3-1" value="<?=$model->v7;?>">
	<input type="hidden" name="v-3-2" id="v-3-2" value="<?=$model->v8;?>">
	<input type="hidden" name="v-3-3" id="v-3-3" value="<?=$model->v9;?>">
	<input type="hidden" name="v-3-4" id="v-3-4" value="<?=$model->v10;?>">
	<input type="hidden" name="v-3-5" id="v-3-5" value="<?=$model->v11;?>">	
	<input type="hidden" name="v3-1" id="v3-1" value="<?=$model->v12;?>">
	<input type="hidden" name="v3-2" id="v3-2" value="<?=$model->v13;?>">
	<input type="hidden" name="v3-3" id="v3-3" value="<?=$model->v14;?>">	
	<input type="hidden" name="v3-4" id="v3-4" value="<?=$model->v15;?>">	
	<input type="hidden" name="v3-5" id="v3-5" value="<?=$model->v16;?>">	
	<input type="hidden" name="v-4-1" id="v-4-1" value="<?=$model->v17;?>">
	<input type="hidden" name="v-4-2" id="v-4-2" value="<?=$model->v18;?>">
	<input type="hidden" name="v-4-3" id="v-4-3" value="<?=$model->v19;?>">
	<input type="hidden" name="v-4-4" id="v-4-4" value="<?=$model->v20;?>">	
	<input type="hidden" name="v-4-5" id="v-4-5" value="<?=$model->v21;?>">		
	<input type="hidden" name="v-5-1" id="v-5-1" value="<?=$model->v22;?>">
	<input type="hidden" name="v-5-2" id="v-5-2" value="<?=$model->v23;?>">
	<input type="hidden" name="v-5-3" id="v-5-3" value="<?=$model->v24;?>">
	<input type="hidden" name="dv-1" id="dv-1" value="<?=$model->v25;?>">
	<input type="hidden" name="dv-2" id="dv-2" value="<?=$model->v26;?>">
	<input type="hidden" name="dv-3" id="dv-3" value="<?=$model->v27;?>">
	<input type="hidden" name="dv-4" id="dv-4" value="<?=$model->v28;?>">
	<input type="hidden" name="dv-5" id="dv-5" value="<?=$model->v29;?>">
	<input type="hidden" name="dv-6" id="dv-6" value="<?=$model->v30;?>">
	<input type="hidden" name="dv-7" id="dv-7" value="<?=$model->v31;?>">
	<input type="hidden" name="dv-8" id="dv-8" value="<?=$model->v32;?>">
	<input type="hidden" name="dv-9" id="dv-9" value="<?=$model->v33;?>">
	<input type="hidden" name="dv-10" id="dv-10" value="<?=$model->v34;?>">
	<input type="hidden" name="dv-11" id="dv-11" value="<?=$model->v35;?>">
	<input type="hidden" name="d-1" id="d-1" value="<?=$model->v36;?>">
	<input type="hidden" name="d-2" id="d-2" value="<?=$model->v37;?>">
	<input type="hidden" name="d-3" id="d-3" value="<?=$model->v38;?>">
	<input type="hidden" name="d-4" id="d-4" value="<?=$model->v39;?>">	
	<input type="hidden" name="d-5" id="d-5" value="<?=$model->v40;?>">
	<input type="hidden" name="d-6" id="d-6" value="<?=$model->v41;?>">		
	<input type="hidden" name="d-7" id="d-7" value="<?=$model->v42;?>">
	<input type="hidden" name="d-8" id="d-8" value="<?=$model->v43;?>">	
	<input type="hidden" name="d-9" id="d-9" value="<?=$model->v44;?>">
	<input type="hidden" name="d-10" id="d-10" value="<?=$model->v45;?>">		
	<input type="hidden" name="d-11" id="d-11" value="<?=$model->v46;?>">
	<input type="hidden" name="d-12" id="d-12" value="<?=$model->v47;?>">	
	<input type="hidden" name="1" id="p1" value="<?=$model->v48;?>">
	<input type="hidden" name="2" id="p2" value="<?=$model->v49;?>">
	<input type="hidden" name="3" id="p3" value="<?=$model->v50;?>">
	<input type="hidden" name="4" id="p4" value="<?=$model->v51;?>">	
	<input type="hidden" name="5" id="p5" value="<?=$model->v52;?>">
	<input type="hidden" name="6" id="p6" value="<?=$model->v53;?>">		
	<input type="hidden" name="7" id="p7" value="<?=$model->v54;?>">
	<input type="hidden" name="8" id="p8" value="<?=$model->v55;?>">	
	<input type="hidden" name="t-1" id="t1" value="<?=$model->v56;?>">
	<input type="hidden" name="t-2" id="t2" value="<?=$model->v57;?>">
	<input type="hidden" name="t-3" id="t3" value="<?=$model->v58;?>">
	<input type="hidden" name="t-4" id="t4" value="<?=$model->v59;?>">	
	<input type="hidden" name="t-5" id="t5" value="<?=$model->v60;?>">
	<input type="hidden" name="t-6" id="t6" value="<?=$model->v61;?>">		
	<input type="hidden" name="t-7" id="t7" value="<?=$model->v62;?>">
	<input type="hidden" name="t-8" id="t8" value="<?=$model->v63;?>">
</form>
