<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Мобильное управление Аккольского района');
?>

<?=$this->registerJs("var _is_index = true;", $this::POS_HEAD)?>

<div class="page-index">

	<div class="wrap wrap1 hidden-sm hidden-xs">
		<div class="container">
			<div class="phones"> <span>1314</span></div> <br />
			<h1><?=Yii::t('app', 'Примите участие<br/>в улучшении нашего района!')?></h1>
			<div class="link">akkol.kz</div>
            <!--<div class="android">
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.zhkh_app" class="sbutton inline white"> <img src="/images/style/googleplay.svg"><b>Google Play</b></a>
            </div>-->

			<div class="scheme inline">
				<div class="item inline">
					<div><img src="/images/style/icon-camera.png"/></div>
					<div class="text"><span><?=Yii::t('app', 'Сделайте <span>снимок</span>')?></span></div>
				</div>
				<div class="delim">
					<img src="/images/style/icon-dots.png"/>
				</div>
				<div class="item inline">
					<div><img src="/images/style/icon-note.png"/></div>
					<div class="text"><span><?=Yii::t('app', '<a href="/report">Подайте</a> <span>заявление</span>')?></span></div>
				</div>
				<div class="delim">
					<img src="/images/style/icon-dots.png"/>
				</div>
				<div class="item inline">
					<div><img src="/images/style/icon-eye.png"/></div>
					<div class="text"><span><?=Yii::t('app', 'Наблюдайте <span>за исполнением</span>')?></span></div>
				</div>
			</div>

		</div>
	</div>

	<div class="wrap wrap1-sm visible-sm visible-xs">
		<div class="container">
			<div class="phones" style="line-height: 40px;"> <span>1314</span></div> <br />
			<h1><?=Yii::t('app', 'Примите участие<br/>в улучшении нашего района!')?></h1>
			<div class="link">akkol.kz</div>

			<div class="scheme ">
				<div class="item ">
					<div><img src="/images/style/icon-camera.png"/></div>
					<div class="text"><span><?=Yii::t('app', 'Сделайте <span>снимок</span>')?></span></div>
				</div>
				<div class="item ">
					<div><img src="/images/style/icon-note.png"/></div>
					<div class="text"><span><?=Yii::t('app', '<a href="/report">Подайте</a> <span>заявление</span>')?></span></div>
				</div>
				<div class="item ">
					<div><img src="/images/style/icon-eye.png"/></div>
					<div class="text"><span><?=Yii::t('app', 'Наблюдайте <span>за исполнением</span>')?></span></div>
				</div>
			</div>

		</div>
	</div>

	<div class="wrap wrap2">
		<div class="container">
			<div class="triangle-white"></div>

			<div class="row">
				<div class="col-sm-6 col-xs-12 item-counter">
					<div class="counter c-alert inline">
						<div class="image"><img src="/images/style/icon-bell.png"/></div>
						<div id="ctr1" class="num"><?=$total?></div>
					</div>
					<div class="text-descr"><?=Yii::t('app', 'Обращений поступило')?></div>
				</div>
				<div class="col-sm-6 col-xs-12 item-counter">
					<div class="counter c-success inline">
						<div class="image"><img src="/images/style/icon-success.png"/></div>
						<div id="ctr2" class="num"><?=$solved?></div>
					</div>
					<div class="text-descr"><?=Yii::t('app', 'Обращений решено')?></div>
				</div>
			</div>

		</div>
	</div>

	<div class="wrap wrap7">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 p-container">
					<div class="product">
						<a href="/event-map">
							<div class="image">
								<img class="img img-responsive" src="/images/style/icon-index-1_black.png"/>
							</div>
							<div class="heading"><?=Yii::t('app', 'Карта событий')?></div>
							<div class="content"><?=Yii::t('app', 'Если Вам интересно узнать, какие работы ведутся рядом с домом, работой или просто посмотреть обращения на карте - добро пожаловать!')?></div>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 p-container">
					<div class="product">
						<a href="/about">
							<div class="image">
								<img class="img img-responsive" src="/images/style/icon-index-2_black.png"/>
							</div>
							<div class="heading"><?=Yii::t('app', 'Работа портала')?></div>
							<div class="content"><?=Yii::t('app', 'О том, как работает портал: как составить обращение, что с ним происходит и кто за этим следит.<br/> :)')?></div>
						</a>
					</div>
				</div>
				<!-- <div class="col-md-4 col-sm-6 p-container">
					<div class="product">
						<a href="/infographics-ouok">
							<div class="image">
								<img class="img img-responsive" src="/images/style/icon-index-3_black.png"/>
							</div>
							<div class="heading"><?=Yii::t('app', 'Работа ОУОК')?></div>
							<div class="content"><?=Yii::t('app', 'Интерактивная демонстрация работы ОУОК, взаимодействие с акиматом, банком, судом.<br/>Полезно знать!')?></div>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 p-container">
					<div class="product">
						<a href="/maek-dashboard">
							<div class="image">
								<img class="img img-responsive" src="/images/style/icon-index-4_black.png"/>
							</div>
							<div class="heading"><?=Yii::t('app', 'Потребление')?></div>
							<div class="content"><?=Yii::t('app', 'Все о воде, которая течет в наших трубах: количесво и качество.<br/>Данные предоставляет ТОО МАЭК')?></div>
						</a>
					</div>
				</div> -->
				<div class="col-md-4 col-sm-6 p-container">
					<div class="product">
						<a href="/stats">
							<div class="image">
								<img class="img img-responsive" src="/images/style/icon-index-5_black.png"/>
							</div>
							<div class="heading"><?=Yii::t('app', 'Статистика')?></div>
							<div class="content"><?=Yii::t('app', 'Статистические показатели за время работы портала')?></div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="wrap wrap3 hidden">
		<div class="container">
			<h2><?=Yii::t('app', 'Уже решенные проблемы')?></h2>

			<div class="row completed-items">
				<?php
					foreach($last_completed as $item) {
						echo $this->render('@app/modules/application/views/application/_index_application', ['model'=>$item]);
					}
				?>
			</div>

			<div class="solved">
				<a href="#"><?=Yii::t('app', 'Архив заявок')?></a>
			</div>

		</div>
	</div>

	<div class="wrap wrap4">
		<div class="container">

			<h3><?=Yii::t('app', 'Поиск управляющей организации вашего дома')?></h3>

			<div class="row">
				<div class="col-sm-12"><?=$this->render('_parts/_house_info', ['addresses'=>$addresses])?></div>
			</div>

		</div>
	</div>

	<div class="wrap wrap5">
		<div class="container">
			<h2><?=Yii::t('app', 'Новости портала')?></h2>

			<div class="row">

				<?php
					foreach($last_news as $ln) {
						echo $this->render('@app/modules/news/views/_parts/_news_block', ['model'=>$ln]);
					}
				?>
			</div>

			<div class="archive">
				<a href="/news"><?=Yii::t('app', 'Архив новостей')?></a>
			</div>

		</div>
	</div>

</div>
