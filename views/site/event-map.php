<?php

	app\assets\EventMapAsset::register($this);

	$this->registerJs("var _categories = ".json_encode($categores), $this::POS_BEGIN);
?>

<div class="wrap white event-map">

	<div class="container">

		<div class="row">
			<div class="col-sm-4">
				<div class="map-switch">
					<a href="/event-map" class="btn custom btn-strainght active"><?=Yii::t('app', 'Активные заявки')?></a>
					<!--<a href="/project-map" class="btn custom btn-strainght"><?/*=Yii::t('app', 'Планируемые работы')*/?></a>-->

					<div class="clearfix">
						<div class="pull-left">
							<button class="link" onclick="hide_all_marks();"><?=Yii::t('app', 'Скрыть все')?></button>
						</div>
						<div class="pull-right">
							<button class="link" onclick="show_all_marks();"><?=Yii::t('app', 'Показать все')?></button>
						</div>
					</div>

				</div>

				<div class="menu-container">
					<div class="menu">
						<?php
							foreach($categores as $cat) {
								echo $this->render('_parts/event-map/_category', ['model'=>$cat]);
							}
						?>
						<div class="scrollbar"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div id="map" class="map"></div>
			</div>
		</div>

	</div>

</div>
