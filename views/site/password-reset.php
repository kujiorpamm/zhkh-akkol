<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Вход в портал');
?>

<div class="page-index">

	<div class="wrap white">
		<div class="container">
			<div class="site-login">

                <?php if (Yii::$app->session->hasFlash('success')) : ?>
                    <h2><?=Yii::t('app', 'Восстановление пароля')?></h2>

        		    <div class="row">
        			    <div class="col-sm-6 col-sm-offset-3">

                            <p style="font-size: 16px;"><?=Yii::$app->session->getFlash('success');?></p>

        			    </div>
        		    </div>
                <?php else: ?>
                    <h2><?=Yii::t('app', 'Восстановление пароля')?></h2>

        		    <div class="row">
        			    <div class="col-sm-6 col-sm-offset-3">
        			    	<?php $form = ActiveForm::begin([
        				        'id' => 'login-form'
        				    ]); ?>

        				        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder'=>'E-mail'])->label(false) ?>

        				        <div class="form-group">
        				           <?= Html::submitButton(Yii::t('app', 'Восстановить'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        				        </div>

        				    <?php ActiveForm::end(); ?>

        				    <div class="register"><?=Yii::t('app', 'Еще не с нами? Присоединяйтесь! <br/><a href="/registration">Регистрация</a>')?></div>

        			    </div>
        		    </div>
                <?php endif; ?>



		</div>
		</div>
	</div>
</div>
