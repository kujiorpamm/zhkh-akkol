<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property string $datetime_from
 * @property string $datetime_to
 * @property string $message_ru
 * @property string $message_kz
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime_from', 'datetime_to', 'message_ru'], 'required'],
            [['datetime_from', 'datetime_to'], 'safe'],
            ['datetime_from','validateDates'],
            [['message_ru', 'message_kz'], 'string'],
        ];
    }

    public function getMessage() {
        switch (Yii::$app->language) {
            case 'kk':
                if($this->message_kz) {
                    return $this->message_kz;
                } else {
                    return $this->message_ru;
                }
                break;

            default:
                return $this->message_ru;
                break;
        }
    }

    public function validateDates(){
        if(strtotime($this->datetime_to) <= strtotime($this->datetime_from)){
            $this->addError('datetime_from','Дата начала должна быть меньше даты конца события');
            $this->addError('datetime_to','Дата начала должна быть меньше даты конца события');
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'datetime_from' => Yii::t('app', 'Дата начала'),
            'datetime_to' => Yii::t('app', 'Дата завершения'),
            'message_ru' => Yii::t('app', 'Сообщение (рус)'),
            'message_kz' => Yii::t('app', 'Сообщение (каз)'),
        ];
    }
}
