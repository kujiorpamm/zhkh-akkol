/*
Counts.js - файл js-скриптов отображащих бульки в трубе на контрольной панели 
Компания разработчик: infographics.kz
Использование настоящей библиотеки без разрешение автора не разрешается
*/

	 
/* ============================= БУЛЬКИ В ТРУБЕ =================================== */
var $bubbles = $('.bubbles'),$b = $('.b');

function bubbles() {
  
  // Settings
   var min_bubble_count = 3, // Minimum number of bubbles
      max_bubble_count = 10, // Maximum number of bubbles
      min_bubble_size = 3, // Smallest possible bubble diameter (px)
      max_bubble_size = 6; // Largest possible bubble diameter (px) 
  
  /*var min_bubble_count = 20, // Minimum number of bubbles
      max_bubble_count = 40, // Maximum number of bubbles
      min_bubble_size = 3, // Smallest possible bubble diameter (px)
      max_bubble_size = 8; // Largest possible bubble diameter (px) */
  
  // Calculate a random number of bubbles based on our min/max
  var bubbleCount = min_bubble_count + Math.floor(Math.random() * (max_bubble_count + 1));
  
  // Create the bubbles
  for (var i = 0; i < bubbleCount; i++) {
    $bubbles.append('<div class="bubble-container"><div class="bubble"></div></div>');
  }
  
  // Now randomise the various bubble elements
  $bubbles.find('.bubble-container').each(function(){
    
    // Randomise the bubble positions (0 - 100%)
    var pos_rand = Math.floor(Math.random() * 101);
    
    // Randomise their size
    var size_rand = min_bubble_size + Math.floor(Math.random() * (max_bubble_size + 1));
    
    // Randomise the time they start rising (0-15s)
    var delay_rand = Math.floor(Math.random() * 16);
    
    // Randomise their speed (3-8s)
    var speed_rand = 1.5 + Math.random() * 2;
    
    // Cache the this selector
    var $this = $(this);
    
    // Apply the new styles
    $this.css({
      'left' : pos_rand + '%',
      
      '-webkit-animation-duration' : speed_rand + 's',
      '-moz-animation-duration' : speed_rand + 's',
      '-ms-animation-duration' : speed_rand + 's',
      'animation-duration' : speed_rand + 's',
      
      '-webkit-animation-delay' : delay_rand + 's',
      '-moz-animation-delay' : delay_rand + 's',
      '-ms-animation-delay' : delay_rand + 's',
      'animation-delay' : delay_rand + 's'
    });
    
    $this.children('.bubble').css({
      'width' : size_rand + 'px',
      'height' : size_rand + 'px'
    });
  });
}

function b() {
  // Settings
   var min_bubble_count = 2, // Minimum number of bubbles
      max_bubble_count = 5, // Maximum number of bubbles
      min_bubble_size = 3, // Smallest possible bubble diameter (px)
      max_bubble_size = 5; // Largest possible bubble diameter (px) 
  // Calculate a random number of bubbles based on our min/max
  var bubbleCount = min_bubble_count + Math.floor(Math.random() * (max_bubble_count + 1));
  // Create the bubbles
  for (var i = 0; i < bubbleCount; i++) {
    $b.append('<div class="bubble-container"><div class="bubble"></div></div>');
  }
  // Now randomise the various bubble elements
  $b.find('.bubble-container').each(function(){
    // Randomise the bubble positions (0 - 100%)
    var pos_rand = Math.floor(Math.random() * 101);
    // Randomise their size
    var size_rand = min_bubble_size + Math.floor(Math.random() * (max_bubble_size + 1));
    // Randomise the time they start rising (0-15s)
    var delay_rand = Math.floor(Math.random() * 16);
    // Randomise their speed (3-8s)
    var speed_rand = 1.1 + Math.random() * 2;
    // Cache the this selector
    var $this = $(this);
    // Apply the new styles
    $this.css({
      'left' : pos_rand + '%',
      '-webkit-animation-duration' : speed_rand + 's',
      '-moz-animation-duration' : speed_rand + 's',
      '-ms-animation-duration' : speed_rand + 's',
      'animation-duration' : speed_rand + 's',
      '-webkit-animation-delay' : delay_rand + 's',
      '-moz-animation-delay' : delay_rand + 's',
      '-ms-animation-delay' : delay_rand + 's',
      'animation-delay' : delay_rand + 's'
    });
    $this.children('.bubble').css({
      'width' : size_rand + 'px',
      'height' : size_rand + 'px'
    });
  });
}
b();
bubbles();
// In case users value their laptop battery life
// Allow them to turn the bubbles off
$('.glass1').hover(function(){
       $bubbles.fadeIn(function(){
		  min_bubble_count = 15; // Minimum number of bubbles
		  max_bubble_count = 30; // Maximum number of bubbles
		  min_bubble_size = 5; // Smallest possible bubble diameter (px)
		  max_bubble_size = 10; 
			bubbles();
			$bubbles.show();
		});
  /*if($bubbles.is(':empty')) { 
  	$bubbles.fadeOut(function(){
		$bubbles.hide();
	});
  } */
  return false;
});
$('.glass1').mouseleave(function() { 
	$bubbles.fadeOut(function(){
		//$bubbles.hide();
		$(this).empty();
		min_bubble_count = 3; // Minimum number of bubbles
		max_bubble_count = 10; // Maximum number of bubbles
		min_bubble_size = 3; // Smallest possible bubble diameter (px)
		max_bubble_size = 6;
		bubbles();
		$bubbles.show();
	});
		
	return false;
});
/*
content = "<font color='red'>Test</font>";
 $(content).appendTo(".bubble-toggle"); */