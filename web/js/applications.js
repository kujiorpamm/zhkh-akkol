$(document).ready(function(){
	moment.locale('ru');
	$('form').each(function(){
	
		$(this).on('beforeSubmit', function (e) {
		    
		    $("[data-krajee-datetimepicker]").each(function(){
		    	var date = $(this).val();
		    	var m_date = moment(date, "DD.MM.YYYY HH:mm:ss").format("YYYY-MM-DD HH:mm:ss");
		    	$(this).val(m_date);
		    });
		    
		    return true;
		});
		
	});
	
	set_dates();
	
	
	/*Кнопка меню для мобилки*/
	$('.xs-menu-trigger').click(function(){
		$('.menu-xs').toggleClass('visible');	
	}); 
		
});


function set_dates() {
	$('.date-counter').each(function(){
		$(this).html( moment($(this).html(), "DD.MM.YYYY HH:mm").fromNow() );	
		$(this).removeClass('date-counter');	
	});
	
	$('.moment-date').each(function(){
		$(this).html( moment($(this).html(), "DD.MM.YYYY").format("LL") );	
		$(this).removeClass('moment-date');		
	});
	
	$('.moment-date-full').each(function(){
		$(this).html( moment($(this).html(), "DD.MM.YYYY HH:mm:ss").format("LL") );	
		$(this).removeClass('moment-date-full');		
	});
	
	$('.moment-date-base').each(function(){
		$(this).html( moment($(this).html()).format("LL") );	
		$(this).removeClass('moment-date-base');		
	});
	
	
	$('.moment-year-month').each(function(){
		$(this).html( moment($(this).html(), "DD.MM.YYYY").format("MMMM, YYYY") );	
		$(this).removeClass('moment-year-month');		
	});
	
}