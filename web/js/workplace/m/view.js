app_coords.reverse();

//mapboxgl.accessToken = 'pk.eyJ1Ijoia3VqaW9ycGFtbSIsImEiOiJjanF6NGdraDMwOW8yM3hwYzF5Z2RtOGt2In0.8WI8WW9RHOLNEm8yeR7YZQ';
mapboxgl.accessToken = 'pk.eyJ1IjoiYmttemMiLCJhIjoiY2phMnppNmVvM2dkMzJ4cGM3bHVjb2ZqZSJ9.M2D4VykfkM_Vffmx0Qp8oA';
    const map = new mapboxgl.Map({
    container: 'map',
    // style: 'mapbox://styles/kujiorpamm/cjrbhx2w11cda2smn1xelqw1i',
    style: '/js/style.json',
    center: app_coords,
    zoom: 8.1
});

map.on('load', function () {

    new mapboxgl.Popup()
            .setLngLat(app_coords)
            .setHTML("Тут!")
            .addTo(map);

});

map.on('click', function(e) {
    return false;
});
