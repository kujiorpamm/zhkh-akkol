$('#ex_btn').click(function(e){
	
	if(document.querySelector('#ex_file').files[0] == undefined) {
		$('#ex_file').addClass('has-error');
	} else {
		getBase64(document.querySelector('#ex_file').files[0]);
		$('#ex_file').removeClass('has-error');
		$('.loader').show();
	}
	
	
	
	
		
});



function getBase64(file) {
   var reader = new FileReader();
   var result;
   reader.readAsDataURL(file);
   reader.onload = function () {
     ajax_request(reader.result);
   };
   reader.onerror = function (error) {
     result = false;
   };;
}

function ajax_request(data) {
	$.ajax({
		url: '/dashboard/admin/load-file',
		type: "POST",
		dataType: "JSON",
		data: {
			blob: data
		},
		success: function(data) {
			for(var key in data) {
				$('#dashboard-'+key).val(data[key]);
			}
			$('.list').hide();
			$('.excel-row').hide();
			$('.loader').hide();
			$('.form-row').show();
		}
	});
}