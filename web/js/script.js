$(document).ready(function(e) {

	$('.confirm-link').each(function(e) {
		$(this).click(function(ev){
			ev.preventDefault();
			$(this).confirmLink();
		});
	});
	same_height();

	$('.btn-group .btn').click(function(e){
		$(this).clickBtnGroup();
	});


	if(typeof _is_index !== 'undefined') {
		var ctr1 = new CountUp("ctr1", 0, $('#ctr1').html());
		var ctr2 = new CountUp("ctr2", 0,  $('#ctr2').html());


		ctr1.start();
		ctr2.start();



	}

	$("#menu-main").on('click', function(){
		$("#menu-sub").toggleClass('hidden');
	});


});

$(document).resize(function(){same_height()});


$.fn.confirmLink = function() {
	if( confirm("Вы уверены?") ) {
		location.href = $(this).attr('href');
	}
}

$.fn.clickBtnGroup = function() {
	$(this).parent().find('.btn').each(function(e) {
		$(this).removeClass('active');
	});
	$(this).addClass('active');
}


function same_height() {
	$(".same-height-parent").each(function(e){
		var container1 = $(this).find(".same-height")[0];
		var container2 = $(this).find(".same-height")[1];

		if($(container1).height() > $(container2).height()) {
			$(container2).css("height", $(container1).height());
		} else {
			$(container1).css("height", $(container2).height());
		}

	});
}

function os_event(evt) {

	if(evt == "change") {
		$(".result span").hide();
		$(".result img").show();
		$(".result").fadeIn();

		$.ajax({
			url: "/ajax/search-organization",
			type: "POST",
			dataType: "JSON",
			data: {
				id: $("#address_id").val()
			},
			success: function(data) {

				$(".result span").html(data.link);
				$(".result span").fadeIn();
				$(".result img").hide();
			},
			error: function(data) {
				var result = $.parseJSON(data.responseText);
				$(".result span").html(result.link);
				$(".result span").fadeIn();
				$(".result img").hide();
			}
		});

	}
}

function error_modal(msg) {
	$('#modal-content').html(msg);
	$('#modal-message').modal('show');
}
