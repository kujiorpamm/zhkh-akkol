<?php

namespace app\modules\user\controllers;

use Yii;
use yii\web\Controller;
use app\modules\user\models\User;
use app\modules\user\models\LoginForm;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

use app\modules\application\models\Application;

/**
 * Default controller for the `user` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['registration', 'login', 'activate'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                    	'actions' => ['profile', 'reset-password'],
                    	'allow' => true,
                    	'roles' => ['@']
                    ]
                    
                ],
            ]
        ];
    }
    
    public function actionRegistration() {
    	$model = new User;
    	$model->scenario = "registration";
    	
    	if($model->load(Yii::$app->request->post())) {			
			$model->password = md5($model->password);
			$model->authkey = uniqid();
			$model->is_active = '0';
			
			if($model->validate()) {
				if($model->save()) {					
					$model->assignRole(); //Дать роль пользователю
					$model->notifyRegistration(); //Уведомить по e-mail о регистрации
					return $this->redirect("/login");
				} else {
					//return $this->render('registration', ['model'=>$model]);
					throw new \yii\web\HttpException(404, Yii::t('app', 'Ошибка сохранения, попробуйте еще раз или обратитесь в службу поддержки.'));
				}
			}			
		}
    	
		return $this->render('registration', ['model'=>$model]);
	}
    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
    //Посмотреть свой профиль
    public function actionProfile() {
		$model = User::findOne(['id'=>Yii::$app->user->id]);
		$model->scenario = "update";
		
		$my_applications = Application::find()->where(['=', 'author', Yii::$app->user->identity->id])->all();
		
		$dataprovider = new ActiveDataProvider([
			'query' => Application::find()->where(['=', 'author', Yii::$app->user->identity->id]),
			'pagination' => [
				'pageSize' => 15
			]
		]);
		
		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {
				return $this->refresh();
			} else {
				//return $this->render('registration', ['model'=>$model]);
				throw new \yii\web\HttpException(404, Yii::t('app', 'Ошибка сохранения, попробуйте еще раз или обратитесь в службу поддержки.'));
			}
		}
		
		return $this->render('profile', ['model'=>$model, 'my_applications'=>$my_applications, 'dp'=>$dataprovider]);
	}
	
	//POST сбросить пароль
	public function actionResetPassword() {
		$model = User::findOne(['id'=>Yii::$app->user->id]);
		if($model->resetPassword()) {
			echo json_encode(['status'=>1, 'message'=>Yii::t('app', 'Пароль успешно изменен. Ваш новый пароль отправлен на почту.')]);
		} else {
			echo json_encode(['status'=>0, 'message'=>Yii::t('app', 'Ошибка. Попробуйте еще раз или обратитесь в службу поддержки.')]);
		}
	}
	
	public function actionActivate($key) {
		if( ($model = User::find()->where(['authkey'=>$key])->one()) !== null) {
			$model->is_active = '1';
			if($model->save()) {
				$model->save();
			} else {
				throw new NotFoundHttpException(Yii::$app->params['error_message']);
				exit;
			}
			return $this->redirect('/login');
		} else {
			throw new NotFoundHttpException(Yii::$app->params['error_message']);
		}
	}
    
}
