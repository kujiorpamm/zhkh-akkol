<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\map\models\MapTypes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Map Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">

    <div class="heading">
        <h3><?= Html::encode($this->title) ?></h3>
	</div>

	<div class="application-container">
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'icon:image',
                'sort',
            ],
        ]) ?>
	</div>

</div>
