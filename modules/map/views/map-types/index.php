<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы объектов на карте';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="workplace">

    <div class="heading">
        <h3><?= Html::encode($this->title) ?></h3>
	</div>

	<div class="application-container">
        <p>
            <?= Html::a('Создать новый', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'icon:image',
                'sort',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
	</div>


</div>
