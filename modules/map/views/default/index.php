<?php

    use app\modules\map\assets\MapAsset;
	widgets\mapbox\MapboxWidget::widget(['page'=>'map-index']);
    // MapAsset::register($this);
    $_geojsons = json_encode($geojsons);
$sources = <<< JS
    var geojsons = $_geojsons;
JS;
    $this->registerJs($sources, $this::POS_HEAD);
    // $this->registerJsFile('/js/map/index.js');

?>



<div class="map-default-index">
    <div id='map' style='width: 100%;'></div>
    <div class="map-layers">
        <?php foreach ($layers as $layer) : ?>
            <a href="#" class="layer-control active" data-layer="<?=$layer->id?>"> <img src="<?=$layer->icon?>" /> <?=$layer->name?></a>
        <?php endforeach; ?>
    </div>

</div>
