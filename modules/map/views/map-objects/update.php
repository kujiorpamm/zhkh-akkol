<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\map\models\MapObjects */

$this->title = 'Редактировать объект: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Map Objects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="workplace">

    <div class="heading">
        <h3><?= Html::encode($this->title) ?></h3>
	</div>

    <div class="application-container">
        <?= $this->render('_form', [
            'model' => $model,
            'types' => $types,
        ]) ?>
    </div>

</div>
