<?php

namespace app\modules\map\assets;

use yii\web\AssetBundle;

class MapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
        'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css',
    ];
    public $js = [
    	'https://api.tiles.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
