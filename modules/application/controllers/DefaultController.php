<?php

namespace app\modules\application\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use kujiorpamm\cropit\widgets\CropitWidget;
use yii\data\ActiveDataProvider;

use app\modules\application\models\Application;
use app\modules\application\models\ApplicationTypes;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationCommends;
use app\modules\application\models\ApplicationFiles;
use app\modules\organization\models\Organization;

/**
 * Default controller for the `application` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['new-application'],
                        'allow' => true,
                        'roles' => ['user', 'admin'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true
                    ],
                    
                ],
            ]
        ];
    }
    
    
    /*Создание заявки обычным пользователем*/
    public function actionNewApplication()
    {
        $model = new Application();
        $model->scenario = 'user-create';
		
		$model->date_created = date('Y-m-d H:i:s');
		$model->date_plane_end = date('Y-m-d H:i:s', strtotime(Yii::$app->params['application']['days_to_add']));
		$model->author = Yii::$app->user->id;
		$model->applicant_name = Yii::$app->user->identity->fname . " " . Yii::$app->user->identity->lname;
		$model->applicant_phone = Yii::$app->user->identity->phone;
		$model->active = 1;
		$model->status = 1;
		
		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {
				
				if($model->image_load) {
					$filename = "images/applications/" . $model->id . ".png";
					CropitWidget::saveAs($model->image_load, $filename);
					$file = new ApplicationFiles;
					$file->type = 1;
					$file->ext = 'png';
					$file->src = "/".$filename;
					$file->application_id = $model->id;
					$file->save();
				}
				
				return $this->redirect("/me");
			}
		}
		
		$allowed_items = ApplicationTypes::find()->select('name_ru')->indexBy('id')->where(['=', 'parent_id', 1])->column();
		return $this->render('new-application', ['model'=>$model, 'category_items'=>$allowed_items]);
        
        return $this->render('index');
    }
    
    public function actionView($id) {
		$model = Application::findOne(['id'=>$id]);
		
		if($model->status == 1) return $this->render('waiting', ['model'=>$model]);
		
		return $this->render('view', ['model'=>$model]);
	}
}
