$(document).ready(function(e){
	checkApplicationTypeForIcon();
	$("#applicationtypes-parent_id").change(function(e){
		checkApplicationTypeForIcon();
	});
	
	
});

//Проверяет нужна ли иконка для заполнения
function checkApplicationTypeForIcon() {
	if($("#applicationtypes-parent_id").val() !== "0") {
		$("#cropit_icon").show();
	} else {
		$("#cropit_icon").hide();
	}
}

function edit_commend(button_edit) {
	let _id = $(button_edit).attr("data-id");
	let _text = $(button_edit).parent().parent().find(".content").html();
	$("#edit_form").find("#applicationcommends-id").val(_id);
	$("#edit_form").find("#commend_edit").val(_text);
	$("#edit_form").find(".redactor-editor").attr("placeholder", "");
	$("#edit_form").find(".redactor-editor").html(_text);
	$('#modal_commend_edit').modal('show');
}