<?php
namespace app\modules\application\assets;

use yii\web\AssetBundle;

class ApplicationAdminAsset extends AssetBundle {
	
	public $sourcePath = '@app/modules/application/assets/';
	public $publishOptions = [
	    'forceCopy' => true,
	];
	
	public $css = [
		'css/style.less'
	];
	
	public $js = [
		'js/admin.js'
	];
	
	public $depends = [ 
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset', 
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset', 
    ];  
	
}

?>