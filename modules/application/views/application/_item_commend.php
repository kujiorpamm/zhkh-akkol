<?php $author = $model->author; ?>

<div class="single-commend">
	<div class="name"><?=$author->organization->name?> <span> - <?=$author->shortname?></span></div>
	<div class="content"><?=$model->text?></div>
	<div class="date moment-date-base"><?=$model->date?></div>

	<?php if(Yii::$app->user->identity && (Yii::$app->user->can('admin') || Yii::$app->user->can('moderator'))): ?>
	<div class="controllers">
		<button class="link" data-id="<?=$model->id?>" onclick="edit_commend(this)"><span class="glyphicon glyphicon-edit"></button>
		<a href="/workplace/moderator/remove-commend?id=<?=$model->id?>" class="confirm-link"><span class="glyphicon glyphicon-remove"></span></a>
	</div>
	<?php endif;?>

</div>
