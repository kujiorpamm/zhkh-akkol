<div class="col-sm-4">
	<div class="item">
		<div class="item-images">
			<div class="holder">
				<a href="/application/<?=$model->id?>"><img class="img img-responsive" src="<?=$model->imageAfter?>"/></a>
			</div>
			<div class="date">
				<?=$model->date_end_human?>
			</div>
			<img class="img img-responsive" src="<?=$model->imageBefore?>"/>
		</div>
		
		<div class="item-name">
			<span> <?=$model->subtype->name?> </span>
		</div>
		
	</div>
</div>