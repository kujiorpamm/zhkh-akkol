<?php	
	use app\modules\application\assets\ApplicationAdminAsset;
	
	ApplicationAdminAsset::register($this);
	$this->title = Yii::t('app', 'Список типов заявок');
	$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">
	
	<div class="heading">
		
		<div class="clearfix">
			<div class="pull-left"><h3><?=Yii::t('app', 'Типы проблем')?></h3></div>
			<div class="pull-right"><button class="btn btn-success pull-right" onclick="js:$('#m_form').modal('show');"><?=Yii::t('app', 'Добавить новый тип')?></button></div>
		</div>
		
	</div>
	
	<div class="application-container">

		<table class="table table-responsive table-parent-child table-app-types">
			<?php
				foreach($all as $app_type) {
					echo "<tr class='parent'> <td colspan=2>{$app_type->name}</td> 
						<td class='col100'>
							<a href='/application/admin/remove-application-type?id={$app_type->id}' class='confirm-link'>
								<span class='glyphicon glyphicon-remove'></span>
							</a>
						</td>
						<td class='col100'>
							<a href='/application/admin/edit-application-type?id={$app_type->id}'>
								<span class='glyphicon glyphicon-eye-open'></span>
							</a>
						</td>
					</tr>";
					
					foreach($app_type->childs as $app_type_child) {
						echo "<tr class='child'> <td class='col100'> <img src='{$app_type_child->icon}'/></td> <td>{$app_type_child->name}</td> 
								<td class='col100'>
									<a href='/application/admin/remove-application-type?id={$app_type_child->id}' class='confirm-link'>
										<span class='glyphicon glyphicon-remove'></span>
									</a>
								</td>
								<td class='col100'>
									<a href='/application/admin/edit-application-type?id={$app_type_child->id}'>
										<span class='glyphicon glyphicon-eye-open'></span>
									</a>
								</td> 
						</tr>";
					}
					
				}
			?>
		</table>
	</div>
	
	
</div>

<?=$this->render('_parts/_app_type_form_modal', ['model'=>$model])?>

