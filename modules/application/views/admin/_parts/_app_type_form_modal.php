<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
?>

<?php	
	
	$types = [0=>'Родительская (содержит остальные)'];
	$types += $model->getMainTypesAsArray();
	
	Modal::begin([
		'id' => 'm_form',
	    'header' => Yii::t('app', 'Добавление нового типа заявок'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ],/*
	    'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->errorSummary($model); ?>
		
		<?=$form->field($model, 'parent_id')->dropDownList($types, ['prompt'=>'Выберите категорию заявки'])->label(false)?>
		<?=$form->field($model, 'name_ru')->textInput(['placeholder'=>$model->getAttributeLabel('name_ru')])->label(false)?>
		<?=$form->field($model, 'name_kz')->textInput(['placeholder'=>$model->getAttributeLabel('name_kz')])->label(false)?>
		
		<div id="cropit_icon">
			<?= $form->field($model, 'icon')->widget(\kujiorpamm\cropit\widgets\CropitWidget::className(), [
				'pluginOptions' => [
					'width' => 30,
					'height' => 30,
					'smallImage' => 'allow'
			]]);?>
		</div>
		
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Добавить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>