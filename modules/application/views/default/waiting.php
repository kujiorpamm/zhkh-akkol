<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\date\DatePicker;

	use kujiorpamm\cropit\widgets\CropitWidget;
	use app\modules\application\assets\ApplicationCreateAsset;
	use app\modules\application\controllers\ApplicationController;

	ApplicationCreateAsset::register($this);

	$this->title = $model->id;
	$this->params['breadcrumbs'][] = Yii::t('app', 'Заявка №{0}', [$model->id]);

	if(Yii::$app->user->identity) {
		$canAdmin = (Yii::$app->user->identity && Yii::$app->user->identity->canAdmin());
	} else {
		$canAdmin = false;
	}



?>

<script>
	var is_view = true;
	var app_coords = [<?=$model->coords?>];
</script>

<div class="page-index">
	<div class="wrap">
		<div class="container">

			<center><?=Yii::t('app', 'Заявка ожидает модерации')?></center>

		</div>
	</div>
</div>
