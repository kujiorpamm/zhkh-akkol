<?php
	$this->title = $model->id;
	/*$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];*/
	/*$this->params['breadcrumbs'][] = Yii::t('app', 'Новая заявка');*/
?>

<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\datetime\DateTimePicker;

	use kujiorpamm\cropit\widgets\CropitWidget;
	widgets\mapbox\MapboxWidget::widget(['page'=>'report']);
	$canAdmin = Yii::$app->user->identity->canAdmin();
?>

<?php $form = ActiveForm::begin(['id'=>'app_form']); ?>

<div class="page-index">
	<div class="wrap">
		<div class="container">

			<h2><?=Yii::t('app', 'Новая заявка')?></h2>

			<div class="row">
			<!--ЛЕВАЯ КОЛОНКА-->
			<div class="col-md-6 col-sm-8 col-xs-12">
				<div class="form-splitter"><?=Yii::t('app', 'Общие данные')?></div>
				<div>
					<!--Подкатегория-->
					<div>
						<?=$form->field($model, 'application_type')->dropDownList($category_items, ['prompt'=>'Выберите категорию'])->label(false)?>
					</div>

					<!--Описание-->
					<div class="form-group">
						<!--<label><?=Yii::t('app', 'Текст заявки')?></label>-->
						<?=$form->field($model, 'description')->textArea(['placeholder'=>Yii::t('app', 'Текст заявки')])->label(false)?>
					</div>
				</div>


				<div class="form-splitter"><?=Yii::t('app', 'Фотография')?> <span class="note glyphicon glyphicon-question-sign"><div>Фотография 800x600 </div></span></div>
				<div class="image-loader">
					<?= $form->field($model, 'image_load')->widget(\kujiorpamm\cropit\widgets\CropitWidget::className(), [
						'pluginOptions' => [
							'width' => 400,
							'height' => 300,
							'smallImage' => 'stretch',
							'exportZoom' => 2
					]])->label(false);?>
				</div>

			</div>
			<!--КОНЕЦ ЛЕВАЯ КОЛОНКА-->

			<!--ПРАВАЯ КОЛОНКА-->
			<div class="col-md-6 col-sm-4 col-xs-12">

				<div class="form-splitter"><label for="Application[coords]"><?=Yii::t('app', 'Координаты')?></label></div>
				<div class="same-height">
					<!--Метка на карте-->
					<div>
						<?=$form->field($model, 'coords')->hiddenInput()->label(false)?>
					</div>

					<div class="map" id="map" style="height:400px;">

					</div>
				</div>

			</div>
			<!--КОНЕЦ ПРАВАЯ КОЛОНКА-->


		</div>

		<div class="clearfix">
			<div class="pull-right">
				<button type="submit" class="btn btn-lg btn-success"> <?=Yii::t('app', 'Отправить заявку')?> </button>
			</div>
		</div>

		<?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
