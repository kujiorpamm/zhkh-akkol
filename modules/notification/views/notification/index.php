<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Уведомления')?></h3>
	</div>

	<div class="application-container dashboard">
		<?= Html::a('Добавить уведомление', ['create'], ['class' => 'btn btn-success']) ?>
	</div>

	<div class="application-container dashboard">
		<div class="panel">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    'datetime_from:text:Дата начала',
                    'datetime_to:text:Дата завершения',
                    'message_ru:ntext:Сообщение (рус)',
                    'message_kz:ntext:Сообщение (каз)',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

		</div>
	</div>

</div>
