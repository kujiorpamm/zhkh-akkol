<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Notifications */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notifications-form">

    <div class="row">
        <div class="col-sm-6">

            <p>
                <div class="alert">
                    Сообщение будет показываться в окне уведомлений на главной странице сайта в диапазоне между первой и второй указанными датами
                </div>
            </p>

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'datetime_from')->widget(DateTimePicker::classname(), [
            	'options' => ['placeholder' => 'Укажите время начала события'],
            	'pluginOptions' => [
            		'autoclose' => true,
                    'language' => 'ru',
                    'format' => 'dd.mm.yyyy hh:ii',
                    'todayBtn' => true
            	]
            ]);?>
            <?= $form->field($model, 'datetime_to')->widget(DateTimePicker::classname(), [
            	'options' => ['placeholder' => 'Укажите время завершения события'],
            	'pluginOptions' => [
            		'autoclose' => true,
                    'language' => 'ru',
                    'format' => 'dd.mm.yyyy hh:ii',
                    'todayBtn' => true
            	]
            ]);?>

            <?= $form->field($model, 'message_ru')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'message_kz')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
