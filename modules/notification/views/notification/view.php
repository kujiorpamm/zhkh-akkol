<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notifications */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Уведомление')?></h3>
	</div>

	<div class="application-container dashboard">
		<div class="panel">

            <p>
                <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы уверены, что хотите удалить?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'datetime_from:datetime',
                    'datetime_to:datetime',
                    'message_ru:ntext',
                    'message_kz:ntext',
                ],
            ]) ?>

		</div>
	</div>

</div>
