var chart1, control, dashboard;

$(document).ready(function(){
	
	google.charts.load('current', {packages: ['corechart', 'controls'], language: 'ru'});
	google.charts.setOnLoadCallback(drawChart1);
	//google.charts.setOnLoadCallback(drawDashboard1);
	
	$('.zoom').click(function(){
		//zoomChart( $(this).attr('data-chart'), $(this).attr('data-app') );	
	});
	
	drawChart2();
		
});


$(window).resize(function(){
	drawChart1();	
});

function drawChart2() {
	var myChart = new Chart($("#mychart"), {
	    type: 'line',
	    data: {
	        labels: [ "2017-01-15", "2017-05-15", "2017-08-14", "2017-08-15"],
	        datasets: [{
	            label: 'tip1',
	            data: [12, 19, 3, 5, 2, 3]
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }],
	            xAxes: [{
	                type: "time",
	                time: {
							format: 'YYYY-MM-DD',
							// round: 'day'
							tooltipFormat: 'll HH:mm'
						},scaleLabel: {
							display: true,
							labelString: 'Date'
						},
	            }]
	        },
	        pan: {
				enabled: true,
				mode: 'xy',
			},
			zoom: {
				enabled: true,
				mode: 'xy'
			},
	    },
	});
}

function zoomChart(elem, dir) {
	var date_max = new Date(eval(chart1.getOption('hAxis.viewWindow.max')));
	
	var date_max = eval(elem).getOption('hAxis.viewWindow.max');
	var ranges = chart1.getDataTable().getColumnRange(0);
	if(date_max == null) {
		date_max = ranges.min;
	}
	console.log(typeof date_max);
	
	
	if(dir == 'plus') date_max.setDate(date_max.getDate() + 30);	
	if(dir == 'minus') date_max.setDate(ranges.max);	
	chart1.setOption('hAxis.viewWindow.min', ranges.min);
	chart1.setOption('hAxis.viewWindow.max', date_max);
	//console.log(date_max);
	chart1.draw();
	
}

function drawDashboard1() {
	var jsonData = $.ajax({
		url: "/statistics/default/ajax-general",
		dataType: "json",
		async: false
	}).responseText;
	result = JSON.parse(jsonData);
	var data = new google.visualization.DataTable(jsonData);
	
	dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));
	
	control = new google.visualization.ControlWrapper({
        controlType: 'ChartRangeFilter',
        containerId: 'control_div',
        options: {
            filterColumnIndex: 0,
            ui: {
                chartOptions: {
                    height: 20,
                    width: '100%',
                    chartArea: {
                        width: '60%'
                    }
                },
                chartView: {
                    columns: [0, 1]
                }
            }
        }
    });
    
    chart1 = new google.visualization.ChartWrapper({
        chartType: 'LineChart',
        containerId: 'chart1',
        options: result.options
    });
    
    dashboard.bind([control], [chart1]);
    dashboard.draw(jsonData);
	
}

function drawChart1() {
	var jsonData = $.ajax({
		url: "/statistics/default/ajax-general",
		dataType: "json",
		async: false
	}).responseText;
	
	result = JSON.parse(jsonData);
	var data = new google.visualization.DataTable(jsonData);

	// Instantiate and draw our chart, passing in some options.
	chart1 = new google.visualization.LineChart(document.getElementById('chart1'));
	chart1.draw(data, result.options);
	
	$('.zoom.in').click(function(){
		result.options.hAxis.viewWindow.min = 'Date(2017,0,1)';	
		result.options.hAxis.viewWindow.max = 'Date(2017,9,1)';	
		chart1.draw(data, result.options);
	});
	$('.zoom.out').click(function(){
		result.options.hAxis.viewWindow.min = 'Date(2017,0,1)';	
		result.options.hAxis.viewWindow.max = 'Date(2017,1,1)';	
		chart1.draw(data, result.options);
	});
	
}