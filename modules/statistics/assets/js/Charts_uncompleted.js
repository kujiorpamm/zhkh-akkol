var chart1,chart2, chart3, chart4, mymap, is_mobile, prev_is_mobile; 


$(window).resize(function(e){
	prev_is_mobile = is_mobile;
	is_mobile = $(window).width() <= 768;
	if(prev_is_mobile != is_mobile) {
		
		
		
	}
});

$(document).ready(function(){
	
	is_mobile = $(window).width() <= 768;
	
	moment.locale('ru');
	
	ajaxChart1();
	ajaxChart2();
	ajaxChart3();
	ajaxChart4();
			
});

function ajaxChart1() {
	$.ajax({
		async: false,
		url: '/statistics/default/ajax-uncompleted-1',
		type: 'POST',
		dataType: 'JSON',
		success: function(data) {
			if(typeof chart1 !== 'undefined') chart1.destroy();
			chart1 = new Chart($("#chart1"), data);
		}	
	});
}
function ajaxChart2() {
	$.ajax({
		async: false,
		url: '/statistics/default/ajax-uncompleted-2',
		type: 'POST',
		dataType: 'JSON',
		success: function(data) {
			if(typeof chart2 !== 'undefined') chart2.destroy();
			chart2 = new Chart($("#chart2"), data);
		}	
	});
}
function ajaxChart3() {
	$.ajax({
		async: false,
		url: '/statistics/default/ajax-uncompleted-3',
		type: 'POST',
		dataType: 'JSON',
		success: function(data) {
			if(typeof chart3 !== 'undefined') chart3.destroy();
			chart3 = new Chart($("#chart3"), data);
		}	
	});
}
function ajaxChart4() {
	$.ajax({
		async: false,
		url: '/statistics/default/ajax-uncompleted-4',
		type: 'POST',
		dataType: 'JSON',
		success: function(data) {
			if(typeof chart4 !== 'undefined') chart4.destroy();
			chart4 = new Chart($("#chart4"), data);
		}	
	});
}

Chart.pluginService.register({
	beforeDraw: function (chart) {
		if (chart.config.options.elements.center) {
    //Get ctx from string
    var ctx = chart.chart.ctx;
    
			//Get options from the center object in options
    var centerConfig = chart.config.options.elements.center;
  	var fontStyle = centerConfig.fontStyle || 'Arial';
			var txt = centerConfig.text;
    var color = centerConfig.color || '#000';
    var sidePadding = centerConfig.sidePadding || 20;
    var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
    //Start with a base font of 30px
    ctx.font = "30px " + fontStyle;
    
			//Get the width of the string and also the width of the element minus 10 to give it 5px side padding
    var stringWidth = ctx.measureText(txt).width;
    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

    // Find out how much the font can grow in width.
    var widthRatio = elementWidth / stringWidth;
    var newFontSize = Math.floor(30 * widthRatio);
    var elementHeight = (chart.innerRadius * 2);

    // Pick a new font size so it will not be larger than the height of label.
    var fontSizeToUse = Math.min(newFontSize, elementHeight);

			//Set font settings to draw it correctly.
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
    ctx.font = fontSizeToUse+"px " + fontStyle;
    ctx.fillStyle = color;
    
    //Draw text in center
    ctx.fillText(txt, centerX, centerY);
		}
	}
});

