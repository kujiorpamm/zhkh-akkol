<?php

namespace app\modules\statistics;

/**
 * statistics module definition class
 */
class module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\statistics\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
