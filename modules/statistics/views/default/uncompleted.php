<?php
	use app\modules\statistics\assets\ChartAsset_uncompleted;
	use app\modules\workplace\assets\WorkplaceAsset;
	
	ChartAsset_uncompleted::register($this);
	WorkplaceAsset::register($this);
	//$this->registerCssFile('@app/modules/workplace/assets/css/base-workplace.less');
	
?>

<div class="workplace default">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Статистика по неисполненным заявкам')?></h3>		
		<div class="button-row">
			<a href="/stats"><?=Yii::t('app', 'Общая')?></a>
			<a class="active" href="/stats-uncompleted"><?=Yii::t('app', 'Неисполненные заявки')?></a>
		</div>
	</div>
	
	<div class="application-container dashboard hidden">
		<div class="panel">
			Всего неисполненных заявок (за все время): <?=$stats['total']?>, из них: <br/>
			- проверяются: <?=$stats['checking']?> <br/>
			- в работе: <?=$stats['working']?> <br/>
			- в заморозке: <?=$stats['frozen']?> <br/>
		</div>
	</div>
	
	<div class="application-container dashboard">
		
		<div class="row">
			<div class="col-sm-6">
				<div class="panel">
					<div class="chart">
						<canvas class="canvas" id="chart1"></canvas>
					</div>
				</div>	
			</div>
			<div class="col-sm-6">
				<div class="panel">
					<div class="chart">
						<canvas class="canvas" id="chart2"></canvas>
					</div>
				</div>	
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-sm-6">
				<div class="panel">
					<div class="chart">
						<canvas class="canvas" id="chart3"></canvas>
					</div>
				</div>	
			</div>
			<div class="col-sm-6">
				<div class="panel">
					<div class="chart">
						<canvas class="canvas high" id="chart4"></canvas>
					</div>
				</div>	
			</div>
		</div>
		
		<div class="panel hidden">
			<b>Более месяца в работе находятся <?=$stats['total_month']?>, из них: </b> </br>
			- проверяются: <?=$stats['checking_month']?> <br/>
			- в работе: <?=$stats['working_month']?> <br/>
			- в заморозке: <?=$stats['frozen_month']?> <br/>
			
			<br/><br/>
			
			<div class="row">
				<div class="col-sm-6">
					<table class="table">
						<thead>
							<tr>
								<th colspan="2">Причины</th>
							</tr>
						</thead>
						<?php foreach($stats['reasons'] as $item) : ?>
						<tr>
							<td><?=$item['uncomplete_reason']?></td>
							<td><?=$item['count']?></td>
						</tr>
						<?php endforeach ?>
					</table>
				</div>
				<div class="col-sm-6">
					<table class="table">
						<thead>
							<tr>
								<th colspan="2">Организации</th>
							</tr>
						</thead>
						<?php foreach($stats['organizations'] as $item) : ?>
						<tr>
							<td><?=$item['name']?></td>
							<td><?=$item['count']?></td>
						</tr>
						<?php endforeach ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>