<?php
	use app\modules\statistics\assets\ChartAsset_bytype;
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
	use kartik\select2\Select2;
	
	ChartAsset_bytype::register($this);
	
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Статистика по типам')?></h3>		
	</div>
	
	<div class="application-container dashboard by-type">
		<div class="row">
			<div class="col-lg-3 col-sm-4">
				<div class="panel">
					<div class="content">
						<?php $form = ActiveForm::begin(); ?>
							<div class="form-group">										
								<?=DatePicker::widget([
								    'model' => $model,
								    'attribute' => 'date1',
								    'attribute2' => 'date2',
								    'options' => ['placeholder' => Yii::t('app', 'От даты')],
								    'options2' => ['placeholder' => Yii::t('app', 'До даты')],
								    'type' => DatePicker::TYPE_RANGE,
								    'form' => $form,
									'separator' => Yii::t('app', 'до'),
								    'pluginOptions' => [
								    	'todayHighlight' => true,
										'todayBtn' => true,
										'format' => 'dd.mm.yyyy',
										'autoclose' => true,
								    ]
								]);?>
							</div>
							
							<div class="form-group">
								<?=$form->field($model, 'application_type')->widget(Select2::classname(), [
								    'data' => $model->types,
								    'options' => ['placeholder' => Yii::t('app', 'Выберите типы'), 'multiple' => true],
								    'pluginOptions' => [
								        'allowClear' => true
								    ],
								])->label(false);?>
							</div>
							
							<div class="form-group">
								<?=$form->field($model, 'address')->widget(Select2::classname(), [
								    'data' => $model->dists,
								    'options' => ['placeholder' => Yii::t('app', 'Выберите районы'), 'multiple' => true],
								    'pluginOptions' => [
								        'allowClear' => true
								    ],
								])->label(false);?>
							</div>
							
							<button class="btn btn-success btn-block"><?=Yii::t('app', 'Применить')?></button>
							
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
			<div class="col-lg-9 col-sm-8">
				<div class="panel fix1">
					<div class="content">
						<h3><span class='date'><?=$model->date1?> </span> - <span class='date'><?=$model->date2?></span></h3>
						
						<div class="row">
							<div class="col-md-2 col-sm-3">
								<div class="stats center">
									<div class="item">
										<div class="value" id="val-1">99</div>
										<div class="text"><?=Yii::t('app', 'Всего<br/>заявок')?></div>
									</div>
								</div>
							</div>
							<div class="col-md-2 col-sm-3">
								<div class="stats center">
									<div class="item">
										<div class="value" id="val-2">99</div>
										<div class="text"><?=Yii::t('app', 'Решено<br/>заявок')?></div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>	
	
</div>