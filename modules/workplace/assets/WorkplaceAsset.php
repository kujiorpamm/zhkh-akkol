<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\workplace\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WorkplaceAsset extends AssetBundle
{    
    public $sourcePath = '@app/modules/workplace/assets';
	public $publishOptions = [
	    'forceCopy' => true,
	];
    
    public $js = [
        'js/workplace.js'
    ];
    
    public $css = [
        'css/base-workplace.less'
    ];
    
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset', 
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset', 
    ];
}
