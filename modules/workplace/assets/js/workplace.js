$(document).ready(function(e){

	$('.btn.act').each(function(){
		$(this).click(function(){
			var attr = $(this).attr('data-attr');
			attr = attr.split(',');

			$('.application-item').each(function(e){
				var app = $(this);
				var has = false;

				for (var tt in attr) {
					if($(app).hasClass(attr[tt])) has = true;
				}

				if(!has) $(app).hide('swing');
				if(has) $(app).show('swing');

			});

		});
	});

	$('.toggler').click(function(e){
		$('.admin-wrap').toggleClass('menu-hidden');
	});

	sideMenu();

});


$(window).resize(function(e){
	sideMenu();
});

function sideMenu() {
	return false;
	if($(window).width() < 1280) {
		$('.admin-wrap').addClass('menu-hidden');
	} else {
		$('.admin-wrap').removeClass('menu-hidden');
	}

}
