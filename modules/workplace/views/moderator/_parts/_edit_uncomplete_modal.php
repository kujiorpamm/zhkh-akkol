<?php
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
?>

<?php	
	Modal::begin([
		'id' => 'm_form_uncompleted',
	    'header' => Yii::t('app', 'Причина неисполнения заявки'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ]/*,
	    'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<?php $form = ActiveForm::begin(['action'=>'/workplace/moderator/set-uncomplete']); ?>
		
		<?=Html::radioList('method', 0, ['Выбрать из списка   ', 'Указать текстом'], ['class'=>'radio', 'id'=>'method_unc'])?>		
		<?=$form->field($model, 'id')->hiddenInput()->label(false)?>
		<br/>
		<div class="method-0">
			<div class="form-group">	
				<?=Html::dropDownList('reason[]', 0, [], ['id'=>'dd_reason', 'class'=>'form-control', 'prompt'=>'- Выберите причину из списка -'])?>
			</div>
		</div>
		
		<div class="method-1 hidden">
			<div class="form-group">			
				<?=Html::textInput('reason[]', '', ['class'=>'form-control'])?>
			</div>
		</div>
		<br/>
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Сохранить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>

<?=$this->registerJs("

	$('#method_unc').change(function(e){
		$('.method-0').toggleClass('hidden');
		$('.method-1').toggleClass('hidden');
	});
	
	$(document).ready(function(){
		$.ajax({
			url : '/workplace/moderator/get-uncomplete-reasons',
			success : function(data) {
				$('#dd_reason').append(data);
			} 	
		});
	});
		

")?>