<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\date\DatePicker;
	use yii\bootstrap\Modal;
	use app\modules\map\assets\MapAsset;

	use kujiorpamm\cropit\widgets\CropitWidget;
	use app\modules\application\models\ApplicationCommends;
	use app\modules\application\assets\ApplicationAdminAsset;
	// use app\modules\application\assets\ApplicationCreateAsset;
	use app\modules\application\controllers\ApplicationController;

	// ApplicationCreateAsset::register($this);
	ApplicationAdminAsset::register($this);
	MapAsset::register($this);
	$this->registerJsFile('/js/workplace/m/view.js', ['position'=>\yii\web\view::POS_END]);

	$this->title = $model->id;
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Заявка №{0}', [$model->id]);

	$canAdmin = Yii::$app->user->identity->canAdmin();

?>

<script>
	var is_view = true;
	var app_coords = [<?=$model->coords?>];
</script>

<div class="workplace archive">

	<div class="heading">
		<h3><?=Yii::t('app', Yii::t('app', 'Заявка №{0} от {1}, планируемое завершение: {2}', [$model->id, substr($model->date_created, 0, 10), substr($model->date_plane_end, 0, 10)]))?></h3>
	</div>

	<div class="application-container">
		<div class="application-view">
			<div class="view-block-container application-info white">
				<div class="heading">
					<div class="clearfix">
						<div class="pull-left"> <?=Yii::t('app', 'Заявка №{0} от {1}, планируемое завершение: {2}', [$model->id, substr($model->date_created, 0, 10), substr($model->date_plane_end, 0, 10)])?></div>
						<div class="pull-right status">
							<?=Yii::t('app', 'Статус:')?> <?=$model->statusName?>
							<?php if($model->status == 3) echo " {$model->date_end}"?>
						</div>
					</div>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-sm-8">
							<div class="type"><?=$model->fulltype?></div>
							<div class="text"><?=$model->description?></div>
							<div class="orgs">
								<!--НАЧАЛО НАЗНАЧЕННЫЕ ИСПОЛНИТЕЛИ-->
								<?=Yii::t('app', 'Назначенные исполнители: ')?>
								<?php
									if($organizations = $model->organizations) {
										foreach($organizations as $org) {
											echo "<a href='/organization/default/view?id={$org->id}'>$org->name</a> ";
										}
									} else {
										echo Yii::t('app', 'Пока нет...');
									}

								?>
								<!--КОНЕЦ НАЗНАЧЕННЫЕ ИСПОЛНИТЕЛИ-->
							</div>
							<div class="author">
								<?=Yii::t('app', 'Автор обращения: ') . "<span>" . $model->authorName?> </span> <br/>
							</div>


						</div>
						<div class="col-sm-4">
							<div class="image">
								<img class="img img-responsive img-popup" src="<?=$model->imageBefore?>"/>
							</div>
						</div>
					</div>
				</div>



			</div>

			<div class="row">
				<div class="col-sm-8">

					<!--НАЧАЛО КОММЕНТАРИИ-->
					<div class="commends-block">
						<h3><?=Yii::t('app', 'Комментарии')?></h3>
						<?php

							if($commends = $model->commends) {
								foreach($model->commends as $cmd) {
									echo ApplicationController::render_commend($cmd);
								}
							} else {
								echo "<div class='empty'>".Yii::t('app', 'Пока нет...')."</div>";
							}


						?>
					</div>

					<?php if(Yii::$app->user->identity->canCommendApplication($model->id)):?>
					<?=$this->render('_parts/_commends', ['application'=>$model, 'model'=>$commend])?>
					<?php else: ?>
					<div class="note"><?=Yii::t('app', 'Вы не можете оставить комментарий, т.к. не находитесь в списке организаций этой заявки')?></div>
					<?php endif; ?>

					<?php
						$model_commend = new ApplicationCommends;
						Modal::begin([
							'id'=>'modal_commend_edit',
							'header' => '<h2>Редактирование комментария</h2>',
							'size' => 'modal-lg',
							'clientOptions' => [
								'show' => false,
								'size' => 'lg'
							]
						]);
						$form = ActiveForm::begin(['action'=>'/workplace/moderator/edit-commend', 'id'=>'edit_form']);
					?>

						<?=$form->field($model_commend, 'id')->hiddenInput()->label(false)?>
						<?= $form->field($model_commend, 'text')->widget(\yii\redactor\widgets\Redactor::className(), [
							'options' => [
								'id' => 'commend_edit',
							],
							'clientOptions' => [
								'plugins' => ['imagemanager','filemanager', 'video'],
								'minHeight' => 500,
								'lang' => 'ru',
								'minHeight' => '300px',
								'buttons' => ['link', 'video', 'image', 'file'],
								'placeholder' => Yii::t('app', 'Ваш комментарий')
							]
						])->label(false);?>

						<div class="clearfix">
							<div class="pull-right">
								<button type="submit" class="btn btn-success"> <?=Yii::t('app', 'Редактировать комментарий')?> </button>
							</div>
						</div>

						<?php ActiveForm::end(); ?>

					<?php Modal::end();?>

					<!--КОНЕЦ КОММЕНТАРИИ-->

				</div>
				<div class="col-sm-4">

					<!--НАЧАЛО КАРТА-->
					<div id="map" class="view-block-container view-block-container application-view-map map"></div>
					<!--КОНЕЦ КАРТА-->

					<!--НАЧАЛО ОРГАНИЗАЦИИ-->
					<div class="view-block-container white executors">
						<div class="heading"><?=Yii::t('app', 'Исполнители')?></div>
						<table class="table no-borders">
							<?php
								if($organizations = $model->assignments) {
									foreach($organizations as $org) {

										if($canAdmin) {
											echo "<tr>
													<td>{$org->organization->name} <span>{$org->contract->name}</span></td>
													<td><a class='confirm-link' href='/workplace/moderator/unasign-organization?id={$model->id}&org={$org->organization->id}'><span class='glyphicon glyphicon-remove'></span></a></td>
												</tr>";
										} else {
											echo "<tr>
													<td>{$org->organization->name} <span>{$org->contract->name}</span></td>
													<td></td>
												</tr>";
										}


									}
								} else {
									echo Yii::t('app', 'Пока нет...');
								}

							?>
						</table>
					</div>
					<!--КОНЕЦ ОРГАНИЗАЦИИ-->

					<!--НАЧАЛО ДАННЫЕ ЗАЯВИТЕЛЯ-->
					<?php if(($model->applicant_name || $model->applicant_addr || $model->applicant_phone) && $canAdmin) {?>

						<div class="view-block-container white">
							<div class="heading"><?=Yii::t('app', 'Данные заявителя')?></div>
							<div><?=$model->applicant_name?></div>
							<div><?=$model->applicant_phone?></div>
							<div><?=$model->applicant_addr?></div>
						</div>
					<?php } ?>
					<!--КОНЕЦ ДАННЫЕ ЗАЯВИТЕЛЯ-->


					<!--ПРИЧИНА НЕИСПОЛНЕНИЯ-->
					<?php if(in_array($model->status, [-1, 0, 1, 2])) : ?>
						<div class="view-block-container white">
							<div class="heading"><?=Yii::t('app', 'Причина неисполнения')?></div>
							<div>
								<?php if($model->uncomplete_reason) echo $model->uncomplete_reason ?>
								<?php if(!$model->uncomplete_reason) echo "Не указана" ?>
							</div>
						</div>
					<?php endif ?>
					<!--КОНЕЦ ПРИЧИНА НЕИСПОЛНЕНИЯ-->

					<!--КНОПКИ УПРАВЛЕНИЯ-->
					<?php if($canAdmin): ?>
					<div class="application-controller">
						<a href="/workplace/moderator/edit-application?id=<?=$model->id?>" class="btn btn-success btn-block"><?=Yii::t('app', 'Редактировать')?></a>
						<button class="btn btn-success btn-block" onclick="$('#m_form_status').modal('show');"><?=Yii::t('app', 'Изменить статус')?></button>

						<!--Добавлять исполнителей можно, только если статус - в работе -->
						<?php if($model->status == 2) : ?>
							<button class="btn btn-success btn-block" onclick="$('#m_form_executors').modal('show');"><?=Yii::t('app', 'Добавить исполнителей')?></button>
						<?php endif ?>

						<!--Добавить причину неисполнения можно, только если заявка не выполнена -->
						<?php if(in_array($model->status, [-1, 0, 1, 2])) : ?>
							<button class="btn btn-success btn-block" onclick="$('#m_form_uncompleted').modal('show');"><?=Yii::t('app', 'Указать причину неисполнения')?></button>
						<?php endif ?>
					</div>
					<?php endif;?>
					<!--КОНЕЦ КНОПКИ УПРАВЛЕНИЯ-->
				</div>
			</div>

		</div>
	</div>


</div>


<?=$this->render('_parts/_edit_status_modal', ['model'=>$model])?>
<?=$this->render('_parts/_edit_executors_modal', ['model'=> $assignment, 'executors'=>$executors])?>
<?=$this->render('_parts/_edit_uncomplete_modal', ['model'=> $model])?>
