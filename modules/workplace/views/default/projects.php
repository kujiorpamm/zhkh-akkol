<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\widgets\ListView;
	use \kop\y2sp\ScrollPager;

	$this->title = Yii::t('app', 'Проекты моей организации');
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Проекты моей организации');
	
	
	
?>

<div class="workplace">
	
	<div class="heading">
		<h3><?=Yii::t('app', 'Проекты моей организации')?></h3>
		
	</div>
	
	<div class="application-container page-project-listing">
		
		<div class="col-md-8">
			<?php
				echo ListView::widget([
				     'dataProvider' => $dp,
				     'itemOptions' => ['class' => 'item'],
				     'itemView' => 'parts/_project_item_view',
				     'pager' => ['class' => ScrollPager::className(),
				     	'paginationSelector' => '.summary',
				     	'enabledExtensions' => [
			                ScrollPager::EXTENSION_TRIGGER,
			                ScrollPager::EXTENSION_SPINNER,
			                ScrollPager::EXTENSION_NONE_LEFT,
			                ScrollPager::EXTENSION_PAGING,
			            ]
			        ]
				]);
			?>
		</div>
		
		
		
	</div>
	
	
	
	
</div>