<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\TimeAsset;
use app\modules\workplace\assets\WorkplaceAsset;

//AppAsset::register($this);
TimeAsset::register($this);
WorkplaceAsset::register($this);

$this->title = Yii::$app->params['siteName'];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?=$this->render('_parts/_header')?>

<div class="wrap admin-wrap">

	<div class="side-menu-container">
		<div class="side-menu">
			<?=$this->render('_parts/_menu')?>
		</div>
		<div class="toggler"></div>
	</div>
	<div class="content-container">
		<?= $content ?>
	</div>

	
</div>

<?=$this->render('@app/views/layouts/_parts/_popup')?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
