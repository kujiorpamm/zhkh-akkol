<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	$this->title = Yii::t('app', 'Редактировать отчет');
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Редактировать отчет');



?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Редактирование отчета')?></h3>
	</div>

	<div class="application-container">

    <?php $form = ActiveForm::begin(); ?>

      <div class="row">
        <div class="col-md-6">
          <?=$form->field($model, 'title_ru')->textInput(['placeholder'=>Yii::t('app', 'Заголовок отчета (рус.)')])->label(false)?>
          <?= $form->field($model, 'text_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
        			'clientOptions' => [
        				'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
        				'minHeight' => 500,
        				'lang' => 'ru',
        				'minHeight' => '300px',
        				'placeholder' => $model->getAttributeLabel('text_ru')
        			]
        		])->label(false);?>
        </div>
        <div class="col-md-6">
          <?=$form->field($model, 'title_kz')->textInput(['placeholder'=>Yii::t('app', 'Заголовок отчета (каз.)')])->label(false)?>
          <?= $form->field($model, 'text_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
        			'clientOptions' => [
        				'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
        				'minHeight' => 500,
        				'lang' => 'ru',
        				'minHeight' => '300px',
        				'placeholder' => $model->getAttributeLabel('text_kz')
        			]
        		])->label(false);?>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <button class="btn btn-lg btn-block btn-success">Сохранить</button>
        </div>
      </div>

    <?php ActiveForm::end(); ?>

	</div>




</div>
