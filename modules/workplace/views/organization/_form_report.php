<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use kartik\date\DatePicker;
?>

<?php $form = ActiveForm::begin(); ?>

  <div class="row">
    <div class="col-xs-3">Дата публикации</div>
    <div class="col-xs-9">
      <?=$form->field($model, 'date')->widget(DatePicker::classname(), [
          'options' => ['placeholder' => Yii::t('app', 'Дата публикации')],
          'pluginOptions' => [
          'todayHighlight' => true,
          'todayBtn' => true,
          'format' => 'yyyy-mm-dd',
          'autoclose' => true,
        ]
      ])->label(false)?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-3">Язык</div>
    <div class="col-xs-9">
      <div class="btn-group">
        <button type="button" data-toggle="tab" href="#rus" class="btn btn-success active">Русский</button>
        <button type="button"data-toggle="tab" href="#kaz" class="btn btn-success">Казахский</button>
      </div>
    </div>
  </div>





  <br />
  <br />
  <div class="tab-content">

    <div id="rus" class="tab-pane fade in active">
      <?=$form->field($model, 'title_ru')->textInput(['placeholder'=>Yii::t('app', 'Заголовок отчета (рус.)')])->label(false)?>
      <?= $form->field($model, 'text_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
          'clientOptions' => [
            'buttons' => ['format', 'bold', 'italic', 'horizontalrule', 'link', 'alignment', 'unorderedlist', 'orderedlist'],
            'minHeight' => 500,
            'lang' => 'ru',
            'minHeight' => '300px',
            'placeholder' => $model->getAttributeLabel('text_ru')
          ]
        ])->label(false);?>
    </div>

    <div id="kaz" class="tab-pane fade">
      <?=$form->field($model, 'title_kz')->textInput(['placeholder'=>Yii::t('app', 'Заголовок отчета (каз.)')])->label(false)?>
      <?= $form->field($model, 'text_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
          'clientOptions' => [
            'buttons' => ['format', 'bold', 'italic', 'horizontalrule', 'link', 'alignment', 'unorderedlist', 'orderedlist'],
            'minHeight' => 500,
            'lang' => 'ru',
            'minHeight' => '300px',
            'placeholder' => $model->getAttributeLabel('text_kz')
          ]
        ])->label(false);?>

    </div>

  </div>

  <button type="submit" class="btn btn-block btn-success"> <?=$model->isNewRecord? "Добавить" : "Обновить" ?></button>

<?php ActiveForm::end(); ?>
