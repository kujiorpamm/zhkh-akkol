<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
  use kartik\grid\GridView;
  $this->title = Yii::t('app', 'Отчеты');
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рабочий стол'), 'url' => ['/workplace']];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Отчеты');



?>

<div class="workplace">

	<div class="heading">
		<h3><?=Yii::t('app', 'Отчеты')?></h3>
	</div>

	<div class="application-container">

    <div class="clearfix p-bottom-20">
      <div class="pull-right">
        <a href="/workplace/organization/new-report" class="btn btn-success">Добавить отчет</a>
      </div>
    </div>

    <?=GridView::widget([
		    'dataProvider'=> $dp,
		    'columns' => [
		    		'id',
		    		'title_ru',
		    		'date',
            [
              'class' => '\kartik\grid\ActionColumn',
              'template' => '{update} {view} {delete}',
              'urlCreator'    => function ($action, $model, $key, $index) {
									if($action == 'view') return \yii\helpers\Url::to(["/organizations/reports/" . $model->id]);
                  return \yii\helpers\Url::to(["/workplace/organization/" . $action ."-report", "id" => $model->id]);
              },
            ]
		    	],
		    'responsive'=>true,
		    'hover'=>true,
		    'striped'=>false,
		    'pjax'=>false,
		    'toolbar'=> false,
		    'export'=>[
		        'fontAwesome'=>false,
		        'options' => [
		        	'class' => 'btn-export',
		        ]
		    ],
		    'exportConfig'=> [
				GridView::EXCEL => true
			],
		    'panel'=>false
		]);?>

	</div>




</div>
