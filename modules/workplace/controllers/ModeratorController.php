<?php

namespace app\modules\workplace\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use kujiorpamm\cropit\widgets\CropitWidget;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

use app\modules\application\models\Application;
use app\modules\application\models\ApplicationTypes;
use app\modules\application\models\ApplicationAssignment;
use app\modules\application\models\ApplicationCommends;
use app\modules\application\models\ApplicationFiles;
use app\modules\organization\models\Organization;


class ModeratorController extends Controller
{


    public $layout = "@workplace/views/layouts/main";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ],
                    [
                    	'allow' => true,
                    	'actions' => ['add-commend'],
                    	'roles' => ['admin', 'moderator', 'organization']
                    ]

                ],
            ]
        ];
    }

    public function actionIndex() {
    	$this->layout = "@workplace/views/layouts/main";

		//$active = Application::find()->where(['IN', 'status', [1, 2]])->orderBy(['id'=>'DESC'])->all(); // Статусы На проверке и В работе

		$active = Application::find()->where(['IN', 'status', [1, 2, 0]])->orderBy(['date_created'=>SORT_DESC])->all(); // Статусы На проверке и В работе
		$active_count = Application::find()->where(['IN', 'status', [1, 2, 0]])->orderBy(['date_created'=>SORT_DESC])->count();
		$new_count = Application::find()->where(['IN', 'status', [1]])->orderBy(['date_created'=>SORT_DESC])->count();
		$work_count = Application::find()->where(['IN', 'status', [2]])->orderBy(['date_created'=>SORT_DESC])->count();
		$frozen_count = Application::find()->where(['IN', 'status', [0]])->count();
		$total_count = Application::find()->count();
		$solved_count = Application::find()->where(['IN', 'status', [3]])->count();
		//var_dump($active_count); exit;

		return $this->render('index', [
			'active'=>$active,
			'active_count'=>$active_count,
			'total_count'=>$total_count,
			'new_count'=>$new_count,
			'work_count'=>$work_count,
			'frozen_count'=>$frozen_count,
			'solved_count'=>$solved_count,
		]);
	}

	public function actionArchive() {

		$this->layout = "@workplace/views/layouts/main";

		if(isset($_POST['search-word'])) $search = $_POST['search-word'];

		if(isset($search)) {
			$query = Application::find()->where(['like', 'description', "{$search}"])
				->orWhere(['like', 'address', "{$search}"])
				->orWhere(['like', 'applicant_name', "{$search}"])
				->orWhere(['like', 'applicant_phone', "{$search}"])
				->orWhere(['like', 'applicant_addr', "{$search}"])
				->orWhere(['=', 'id', $search]);
		} else {
			$query = Application::find();
		}

		//echo "<pre>"; print_r($query->createCommand()->queryAll()); echo "</pre>"; exit;

		$dp = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
			'sort' => [
				'defaultOrder' => [
					'date_end' => SORT_DESC,
					'date_created' => SORT_DESC,
					'date_created_human' => SORT_DESC,
				],
				'attributes' => [
					'date_end' => [
						'desc' => ['date_end'=>SORT_DESC],
						'asc' => ['date_end'=>SORT_ASC],
					],
					'date_created_human' => [
						'desc' => ['date_created'=>SORT_DESC],
						'asc' => ['date_created'=>SORT_ASC],
					],
					'statusName' => [
						'desc' => ['status'=>SORT_DESC],
						'asc' => ['status'=>SORT_ASC],
					]
				]
			]
		]);

		return $this->render('archive', ['dp'=>$dp, 'search'=>$search]);
	}

	public function actionView($id) {
    	$this->layout = "@workplace/views/layouts/main";

		$model = Application::findOne(['id'=>$id]);
		$model_assignment = new ApplicationAssignment;
		$commend = new ApplicationCommends;
		$commend->application_id = $model->id;
		$model_assignment->application_id = $model->id;
		$model_assignment->application_end = $model->date_plane_end;
		$executors = Organization::find()->select(['name'])->indexBy('id')->column();

		return $this->render('view', ['model'=>$model, 'assignment'=>$model_assignment, 'executors'=>$executors, 'commend'=>$commend]);
	}

	public function actionNewApplication() {
		$model = new Application();

		$model->date_created = date('d.m.Y H:i:s');
		$model->date_plane_end = date('d.m.Y H:i:s', strtotime(Yii::$app->params['application']['days_to_add']));
		$model->author = Yii::$app->user->id;
		$model->active = 1;
		$model->status = 1;

		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {

				if($model->image_load) {
					$filename = "images/applications/" . $model->id . ".png";
					CropitWidget::saveAs($model->image_load, $filename);
					$file = new ApplicationFiles;
					$file->type = 1;
					$file->ext = 'png';
					$file->src = "/".$filename;
					$file->application_id = $model->id;
					$file->save();
				}

				return $this->redirect("/workplace/moderator");
			}
		}

		$category_items = ApplicationTypes::getMainTypesAsArray();
		return $this->render('new-application', ['model'=>$model, 'category_items'=>$category_items]);
	}

	public function actionEditApplication($id) {

		$model = Application::findOne(['id'=>$id]);
		//echo "<pre>"; print_r(Yii::$app->request->post()); echo "</pre>"; exit;
		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {

				if($model->image_load) {
					$filename = "images/applications/" . $model->id . "___" . rand() . ".png";
					CropitWidget::saveAs($model->image_load, $filename);
					$file = new ApplicationFiles;
					$file->type = 1;
					$file->ext = 'png';
					$file->src = "/".$filename;
					$file->application_id = $model->id;
					$file->save();
				}

				return $this->redirect("/workplace/moderator/edit-application?id=".$model->id);
			}
		}

		$model->main_category = $model->type->id;

		$subcategories = ApplicationTypes::find()->select('name_ru')->indexBy('id')->where(['=', 'parent_id', $model->main_category])->column();
		$category_items = ApplicationTypes::getMainTypesAsArray();
		return $this->render('edit-application', ['model'=>$model, 'category_items'=>$category_items, 'subcategory_items'=>$subcategories]);
	}

	public function actionChangeStatus() {

		$model = Application::findOne(['id'=>$_POST['Application']['id']]);
		if($model->load(Yii::$app->request->post())) {

			if($model->status == 3) {
				$model->date_end = date('Y-m-d H:i:s');
				if($model->image_load) {
					$filename = "images/applications/" . $model->id . "___" . rand() . ".png";
					CropitWidget::saveAs($model->image_load, $filename);
					$file = new ApplicationFiles;
					$file->type = 2;
					$file->ext = 'png';
					$file->src = "/".$filename;
					$file->application_id = $model->id;
					$file->save();
				}
			}

			$model->save();
			return $this->redirect(Yii::$app->request->referrer);
		}

	}

	//TODO:: Нужно перехватывать ошибки и выдавать сообщение, если назначение не произошло
	public function actionAssignOrganization() {
		$model = new ApplicationAssignment;
		if($model->processAssignment( Yii::$app->request->post() )) {
			return $this->redirect(Yii::$app->request->referrer);
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionUnasignOrganization($id, $org) {
		ApplicationAssignment::find()->where(['=','application_id',$id])->andWhere(['=', 'organization_id', $org])->one()->delete();
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionAddCommend() {
		$model = new ApplicationCommends;
		if($model->load(Yii::$app->request->post())) {
			$model->save();
			return $this->redirect(Yii::$app->request->referrer);
		} else {

		}
	}

	public function actionEditCommend() {
		$model = ApplicationCommends::findOne(['id'=>$_POST['ApplicationCommends']['id']]);
		if($model) {
			$model->text = $_POST['ApplicationCommends']['text'];
			if($model->save()) {
				return $this->redirect(Yii::$app->request->referrer);
			} else {
				throw new \yii\web\NotFoundHttpException();
			}
		} else {
			throw new \yii\web\NotFoundHttpException();
		}
	}

	public function actionRemoveCommend($id) {
		ApplicationCommends::findOne(['id'=>$id])->delete();
		return $this->redirect(Yii::$app->request->referrer);
	}

	/*Незавершенные заявки*/

	public function actionGetUncompleteReasons() {
		$sql = "SELECT uncomplete_reason FROM application GROUP BY uncomplete_reason";
		$result = Yii::$app->db->createCommand($sql)->queryAll();
		$html = "";
		foreach($result as $value) {
			if($value['uncomplete_reason'] == null) continue;
			$html .= "<option value='{$value['uncomplete_reason']}'>{$value['uncomplete_reason']}</option>";
		}
		echo $html;
	}

	public function actionSetUncomplete() {
		$model = Application::findOne(['id'=>$_POST['Application']['id']]);
		$model->uncomplete_reason = ($_POST['method'] == 0) ? $_POST['reason'][0] : $_POST['reason'][1];
		if($model->uncomplete_reason == "") $model->uncomplete_reason = null;
		$model->save();
		return $this->redirect(Yii::$app->request->referrer);
	}

}
