<?php

?>

<div class="page-index">

	<div class="wrap white">
		<div class="container">		
			<h2><?=Yii::t('app', 'Список организаций работающих в портале')?></h2>
			
			<div class="column3">
				<?php
					foreach($models as $model) {
						echo "<a class='inline-block black size-md' href='/organizations/{$model->id}'>$model->name</a> <br/>";
					}
				?>
			</div>
			
		</div>
	</div>
	
</div>
