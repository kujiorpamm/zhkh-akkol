<?php

use app\modules\organization\assets\OrganizationAsset;

OrganizationAsset::register($this);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Организации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view-organization', 'id'=>$model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Список услуг');
?>

<div class="workplace">
	
	<div class="heading">
		<div class="clearfix">
			<div class="pull-left">
				<h3><?=Yii::t('app', $model->name)?></h3>
				<?=$this->render('_parts/_organization_submenu', ['model'=>$model])?>
			</div>
			<div class="pull-right">
				<button class="btn btn-success" onclick="js:$('#m_form').modal('show');">
					<?=Yii::t('app', 'Добавить услугу')?>
				</button>
			</div>
		</div>		
	</div>
	
	<div class="application-container">
		<table class="table">
			<thead>
				<tr>
					<th><?=Yii::t('app', 'Наименование')?></th>
					<th><?=Yii::t('app', 'Описание')?></th>
					<th><?=Yii::t('app', '')?></th>
					<th><?=Yii::t('app', '')?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					
					foreach($model->services as $service) {
						echo "
							<tr>
								<td>{$service->name}</td>
								<td>{$service->price}</td>
								<td>
									<a href='/organization/admin/remove-service?id={$service->id}' class='confirm-link'>
										<span class='glyphicon glyphicon-remove'></span>
									</a>
								</td>
								<td>
									<a href='/organization/admin/edit-service?id={$service->id}'>
										<span class='glyphicon glyphicon-eye-open'></span>
									</a>
								</td>
							</tr>
						";
					}
					
				?>
			</tbody>
		</table>
	</div>
	
	
</div>



<?=$this->render('_parts/_organization_service_add_modal', ['model'=>$service])?>