<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kujiorpamm\cropit\widgets\CropitWidget;
?>


<?php
	Modal::begin([
		'id' => 'm_form',
	    'header' => Yii::t('app', 'Добавление организации'),
	    'size' => 'modal-lg',
		'options' => [
			'tabindex' => false
		]
		/*'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>

	<?php $form = ActiveForm::begin(); ?>
	<?= $form->errorSummary($model); ?>

		<div class="row">
			<div class="col-xs-2">
				<p class="bold"><?=$model->getAttributeLabel('name')?>*</p>
			</div>
			<div class="col-xs-10">
				<?=$form->field($model, 'name')->textInput(['class'=>'form-control'])->label(false)?>
			</div>
		</div>


		<div class="row">
			<div class="col-xs-2">
				<p class="bold"><?=$model->getAttributeLabel('contact_ru')?>*</p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'contact_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2">
				<p><?=$model->getAttributeLabel('contact_kz')?></p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'contact_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2">
				<p class="bold"><?=$model->getAttributeLabel('activity_ru')?>*</p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'activity_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2">
				<p><?=$model->getAttributeLabel('activity_kz')?></p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'activity_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2">
				<p><?=$model->getAttributeLabel('heading_ru')?></p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'heading_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2">
				<p><?=$model->getAttributeLabel('heading_kz')?></p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'heading_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2">
				<p><?=$model->getAttributeLabel('rule_ru')?></p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'rule_ru')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-2">
				<p><?=$model->getAttributeLabel('rule_kz')?></p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'rule_kz')->widget(\yii\redactor\widgets\Redactor::className(), [
					'clientOptions' => [
						'plugins' => ['fontcolor','imagemanager','filemanager', 'video'],
						'minHeight' => 500,
						'lang' => 'ru',
						'minHeight' => '300px'
					]
				])->label(false);?>
			</div>
		</div>



		<div class="row">
			<div class="col-xs-2">
				<p><?=$model->getAttributeLabel('logo')?></p>
			</div>
			<div class="col-xs-10">
				<?= $form->field($model, 'logo')->widget(CropitWidget::className(), [
					'pluginOptions' => [
				        'width' => 300,
				        'height' => 300,
				        'smallImage' => 'allow',
				        'exportZoom' => 1
				    ]
				])->label(false) ?>
			</div>
		</div>

		<center><button type="submit" class="btn btn-success"> <?=Yii::t('app', 'Добавить')?> </button></center>

	<?php ActiveForm::end(); ?>


<?php
	Modal::end();
?>
