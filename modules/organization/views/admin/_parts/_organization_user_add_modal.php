<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kujiorpamm\cropit\widgets\CropitWidget;
?>
	
	
<?php	
	Modal::begin([
		'id' => 'm_form',
	    'header' => Yii::t('app', 'Добавление пользователя в организацию'),
	    'size' => 'modal-md',
	    'options' => [
	    	'tabindex' => false
	    ],
	    /*'clientOptions'=> [
	    	'show' => true
	    ]*/
	]);
?>
	
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->errorSummary($model); ?>
		
		<?=$form->field($model, 'organization_id')->hiddenInput()->label(false)?>
		<?=$form->field($model, 'user_id')->widget(\kartik\select2\Select2::className(), [
			'data' => $users,
			'options' => ['placeholder' => Yii::t('app', 'Выберите пользователя')],
		
		])->label(false)?>
		<?php //$form->field($model, 'commend')->textArea(['placeholder'=>'Комментарий к пользователю, например - Иван Иванович из МАЭКА, директор'])->label(false)?>
		
		<button type="submit" class="btn btn-success btn-block"> <?=Yii::t('app', 'Добавить')?> </button>
		
	<?php ActiveForm::end(); ?>
	
	
<?php
	Modal::end();
?>
