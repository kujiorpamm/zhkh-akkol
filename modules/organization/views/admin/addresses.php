<div class="workplace">
	
	<div class="heading">
		<h3><?=$model->fullname?></h3>
		
	</div>
	
	<div class="application-container">
				
		<div class="clearfix">
			
			<div class="pull-left">
				<img src="/images/style/address-type-0.png"/> - Улица, микрорайон &nbsp; &nbsp; &nbsp; &nbsp;
				<img src="/images/style/address-type-1.png"/> - Дом, здание
			</div>
			
			<div class="pull-right">
				<button class="btn btn-success pull-right" onclick="js:$('#m_form').modal('show');"><?=Yii::t('app', 'Добавить адрес')?></button>
			</div>
		</div>
		<br/>
		<table class="table">
			<thead>
				<tr>
					<th></th>
					<th><?=Yii::t('app', 'Наименование')?></th>
					<th><?=Yii::t('app', 'Полный адрес')?></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					if($model->parent) {
						echo "<tr><td colspan='4'><a href='/organization/admin/addresses?id={$model->parent->id}'> < вернуться </a></td></tr>";
					}
				?>
				<?php
					
					foreach($model->childs as $child) {
						echo "
							<tr>
								<td><img src='/images/style/address-type-{$child->type}.png'/></td>
								<td><a href='/organization/admin/addresses?id={$child->id}'>{$child->name}</a></td>
								<td>{$child->fullname}</td>
								<td>
									<a href='/organization/admin/remove-address?id={$child->id}' class='confirm-link'>
										<span class='glyphicon glyphicon-remove'></span>
									</a>
								</td>
								<td>
									<a href='/organization/admin/edit-address?id={$child->id}'>
										<span class='glyphicon glyphicon-eye-open'></span>
									</a>
								</td>
							</tr>
						";
					}
					
				?>
				<?php
					if($model->parent) {
						echo "<tr><td colspan='3'><a href='/organization/admin/addresses?id={$model->parent->id}'> < вернуться </a></td></tr>";
					}
				?>
			</tbody>
		</table>
	</div>
	
	
</div>


<?=$this->render('_parts/_addresses_add_modal', ['model'=>$new_model])?>