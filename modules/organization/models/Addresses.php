<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "addresses".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $parent_id
 * @property string $name
 * @property string $fullname
 * @property integer $organization_id
 */
class Addresses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name', 'fullname'], 'required'],
            [['type', 'parent_id', 'organization_id'], 'integer'],
            [['name', 'fullname'], 'string', 'max' => 100],
            [['fullname'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'name' => Yii::t('app', 'Наименование'),
            'fullname' => Yii::t('app', 'Полное наименование'),
            'organization_id' => Yii::t('app', 'Organization ID'),
        ];
    }
    
    public function getChilds() {
		return $this->find()->where(['parent_id'=>$this->id])->orderBy(['name'=>SORT_ASC])->all();
	}
	
	public function getParent() {
		return $this->findOne(['id'=>$this->parent_id]);
	}
	
	public static function asArray() {
		$models = self::find()->where(['type'=>1])->andWhere(['IS  NOT', 'organization_id', NULL])->all();
		foreach($models as $model) {
			$newarr[$model->id] = $model->fullname;
		}
		return $newarr;
	}
    
}
