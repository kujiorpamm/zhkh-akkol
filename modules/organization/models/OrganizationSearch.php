<?php

namespace app\modules\organization\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\organization\models\Organization;

/**
 * OrganizationSearch represents the model behind the search form about `app\modules\organization\models\Organization`.
 */
class OrganizationSearch extends Organization
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contact_kz'], 'integer'],
            [['name', 'activity_ru', 'activity_kz', 'heading_ru', 'heading_kz', 'rule_ru', 'rule_kz', 'contact_ru', 'logo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Organization::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_kz' => $this->contact_kz,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'activity_ru', $this->activity_ru])
            ->andFilterWhere(['like', 'activity_kz', $this->activity_kz])
            ->andFilterWhere(['like', 'heading_ru', $this->heading_ru])
            ->andFilterWhere(['like', 'heading_kz', $this->heading_kz])
            ->andFilterWhere(['like', 'rule_ru', $this->rule_ru])
            ->andFilterWhere(['like', 'rule_kz', $this->rule_kz])
            ->andFilterWhere(['like', 'contact_ru', $this->contact_ru])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
}
