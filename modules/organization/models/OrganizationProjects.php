<?php

namespace app\modules\organization\models;

use Yii;
use app\modules\organization\models\Organization;

/**
 * This is the model class for table "organization_projects".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_kz
 * @property string $descr_ru
 * @property string $descr_kz
 * @property string $coords
 * @property string $date_1
 * @property string $date_2
 * @property integer $organization_id
 */
class OrganizationProjects extends \yii\db\ActiveRecord
{
    
    public $status;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization_projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'descr_ru', 'coords', 'date_1', 'date_2', 'organization_id'], 'required'],
            [['descr_ru', 'descr_kz'], 'string'],
            [['date_1', 'date_2'], 'safe'],
            [['organization_id'], 'integer'],
            [['name_ru', 'name_kz', 'coords'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Наименование (рус)'),
            'name_kz' => Yii::t('app', 'Наименование (каз)'),
            'descr_ru' => Yii::t('app', 'Описание (рус)'),
            'descr_kz' => Yii::t('app', 'Описание (каз)'),
            'coords' => Yii::t('app', 'Координаты'),
            'date_1' => Yii::t('app', 'Дата начала'),
            'date_2' => Yii::t('app', 'Дата завершения'),
            'organization_id' => Yii::t('app', 'Organization ID'),
        ];
    }
    
    public function getOrganization() {
		return $this->hasOne(Organization::className(), ['id'=>'organization_id']);
	}
    
    public function getName() {
		return $this->name_ru;
	}
    
    public function getDescr() {
		return $this->descr_ru;
	}
	
	public function afterFind() {
		if(date('Y-m-d H:i:s') >= $this->date_1) {
			$this->status = 'now';
			return;	
		};
		if(date('Y-m-t') >= $this->date_1) {
			$this->status = 'this_month';
			return;	
		};
		$this->status = 'later';
	}
}
