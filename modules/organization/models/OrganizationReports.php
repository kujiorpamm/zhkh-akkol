<?php

namespace app\modules\organization\models;

use Yii;
use yii\data\ActiveDataProvider;


use app\modules\organization\models\Organization;
use app\models\Files;


/**
 * This is the model class for table "organization_reports".
 *
 * @property integer $id
 * @property integer $organization_id
 * @property string $title_ru
 * @property string $title_kz
 * @property string $text_ru
 * @property string $text_kz
 * @property string $date
 * @property integer $author_id
 */
class OrganizationReports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization_reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'title_ru', 'text_ru', 'date', 'author_id'], 'required'],
            [['organization_id', 'author_id'], 'integer'],
            [['text_ru', 'text_kz'], 'string'],
            [['date'], 'safe'],
            [['title_ru', 'title_kz'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'ID организации'),
            'title_ru' => Yii::t('app', 'Заголовок (рус)'),
            'title_kz' => Yii::t('app', 'Заголовок (каз)'),
            'text_ru' => Yii::t('app', 'Текст (рус)'),
            'text_kz' => Yii::t('app', 'Текст (каз)'),
            'date' => Yii::t('app', 'Дата'),
            'author_id' => Yii::t('app', 'ID автора'),
        ];
    }

    public function getTitle() {
      if(Yii::$app->language == 'ru') {
        return $this->title_ru;
      } else if (Yii::$app->language == 'kk' && $this->title_kz) {
        return $this->title_kz;
      }
      return $this->title_ru;
    }

    public function getText() {
      if(Yii::$app->language == 'ru') {
        return $this->text_ru;
      } else if (Yii::$app->language == 'kk' && $this->text_kz) {
        return $this->text_kz;
      }
      return $this->text_ru;
    }

    public function getOrganization() {
      return $this->hasOne(Organization::className(), ['id'=>'organization_id']);
    }

    public static function getOrganizationDP() {
      return new ActiveDataProvider([
  			'query' => self::find()->where(['organization_id'=>Yii::$app->user->identity->organization->id]),
  			'pagination' => [
  				'pageSize' => 10,
  			],
  			'sort' => [
  				'defaultOrder' => [
  					'date' => SORT_DESC
  				]
  			]
      ]);
    }

    public function search() {
      $query = self::find();
      if($this->organization_id) $query->where(['organization_id'=>$this->organization_id]);
      return new ActiveDataProvider([
  			'query' => $query,
  			'pagination' => [
  				'pageSize' => 10,
  			],
  			'sort' => [
  				'defaultOrder' => [
  					'date' => SORT_DESC
  				]
  			]
      ]);
    }

    public function getFiles() {
      return $this->hasMany(Files::className(), ['table_id'=>'id'])->where(['table_name'=>self::tableName()]);
    }

}
