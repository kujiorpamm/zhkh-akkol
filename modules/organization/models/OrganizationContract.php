<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "organization_contract".
 *
 * @property integer $id
 * @property integer $organization_id
 * @property string $name
 * @property string $description
 * @property string $start
 * @property string $end
 */
class OrganizationContract extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization_contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'name', 'start', 'end', 'email_view'], 'required'],
            [['organization_id'], 'integer'],
            [['start', 'end'], 'safe'],
            [['name'], 'string', 'max' => 500],
            [['email_view'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'name' => Yii::t('app', 'Наименование'),
            'description' => Yii::t('app', 'Описание'),
            'start' => Yii::t('app', 'Начинается'),
            'end' => Yii::t('app', 'Заканчивается'),
            'email_view' => Yii::t('app', 'Шаблон письма'),
        ];
    }
    
    public function beforeSave($insert) {
		if(parent::beforeSave($insert)) {
			
			$this->start = Yii::$app->formatter->asDate($this->start, 'yyyy-MM-dd');
			$this->end = Yii::$app->formatter->asDate($this->end, 'yyyy-MM-dd');
			
			return true;
		} else {
			return false;
		}
	}
    
}
