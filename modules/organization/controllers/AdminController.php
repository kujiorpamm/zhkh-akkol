<?php

namespace app\modules\organization\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\user\models\User;
use app\modules\organization\models\Addresses;
use app\modules\organization\models\Organization;
use app\modules\organization\models\OrganizationUsers;
use app\modules\organization\models\OrganizationSearch;
use app\modules\organization\models\OrganizationContract;
use app\modules\organization\models\OrganizationServices;
use kujiorpamm\cropit\widgets\CropitWidget;

/**
 * Default controller for the `user` module
 */
class AdminController extends Controller
{
    public $layout = "@app/modules/workplace/views/layouts/main";
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'moderator'],
                    ]
                    
                ],
            ]
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new OrganizationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = new Organization();
		
		if($model->load(Yii::$app->request->post())) {
			
			if($model->logo) {
				$path = "uploads/images/orgs/" . md5(date("YmdHis")) . ".png";
				if( CropitWidget::saveAs($model->logo, $path) ) {
					$model->logo = "/".$path;
				}
			}
			
			if($model->save()) {
				return $this->refresh();
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		}
		
		
        return $this->render('index', [
        	'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionViewOrganization($id) {
		$model = $this->findModel($id);
		
		if($model->load(Yii::$app->request->post())) {
			
			if($model->logo) {
				$path = "uploads/images/orgs/" . md5(date("YmdHis")) . ".png";
				if( CropitWidget::saveAs($model->logo, $path) ) {
					$model->logo = "/".$path;
				}
			}
			
			if($model->save()) {
				return $this->refresh();
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
		}
				
		return $this->render('view-organization', ['model'=>$model]);
		
	}
    
    
    //todo: Пользователь может быть только в одной организации
    public function actionOrganizationUsers($id) {
		$model = $this->findModel($id);
		
		$model_user = new OrganizationUsers();
		$model_user->organization_id = $model->id;
		$users = User::findByRole(['organization', 'admin', 'moderator', 'maek', 'ouok', 'controller'], true);
		
		if($model_user->load(Yii::$app->request->post())) {
			$model_user->save();
			return $this->refresh();
		}
								
		return $this->render('organization-users', [
			'model'=>$model,
			'model_user' => $model_user,
			'users' => $users
		]);
		
	}
	
	public function actionOrganizationContracts($id) {
		$model = $this->findModel($id);
		
		$model_contract = new OrganizationContract();
		$model_contract->organization_id = $model->id;
		
		if($model_contract->load(Yii::$app->request->post())) {
			if($model_contract->save()) {
				return $this->refresh();
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
		
		return $this->render('organization-contracts', [
			'model' => $model,
			'model_contract' => $model_contract
		]);
		
	}
	
	public function actionEditContract($id) {
		$model = OrganizationContract::findOne(['id'=>$id]);
		
		if($model->load(Yii::$app->request->post())) {
			if($model->save()) {
				return $this->redirect('/organization/admin/organization-contracts?id='.$id);
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
		
		return $this->render('organization-contract-edit', ['model'=>$model]);
	}
	
	//todo: контракты нужно скрывать, а не удалять - чтобы не было ошибок в бд
	public function actionRemoveContract($id) {
		return $this->redirect(Yii::$app->request->referrer);
	}
	
	public function actionUnlinkUser($id) {
		OrganizationUsers::deleteAll(['user_id'=>$id]);
		return $this->redirect(Yii::$app->request->referrer);
	}
	
	public function actionUnlinkAddress($id) {
		$model = Addresses::findOne(['id'=>$id]);
		$model->organization_id = null;
		$model->save();
		return $this->redirect(Yii::$app->request->referrer);
	}
	
	/**
	* 	Адреса
	*/
	public function actionAddresses($id=7) {
		$model = Addresses::findOne(['id'=>$id]);
		
		$new_model = new Addresses();
		$new_model->parent_id = $model->id;
		
		if($new_model->load(Yii::$app->request->post())) {
			$new_model->fullname = $new_model->parent->fullname . ', ' . $new_model->name;
			if($new_model->save()) {
				return $this->refresh();
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
		
		return $this->render('addresses', ['model'=>$model, 'new_model'=>$new_model]);
	}
	
	public function actionEditAddress($id) {
		$model = Addresses::findOne(['id'=>$id]);
		
		if($model->load(Yii::$app->request->post())) {
			$model->fullname = $model->parent->fullname . ', ' . $model->name;
			if($model->save()) {
				return $this->redirect('/organization/admin/addresses?id='.$model->parent->id);
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
		return $this->render('address-edit', ['model'=>$model]);
	}
	
	// todo:: Должны удаляться все ссылки на эти адреса
	public function actionRemoveAddress($id) {
		Addresses::findOne(['id'=>$id])->delete();
		return $this->redirect(Yii::$app->request->referrer);
	}
	
	public function actionOrganizationAddresses($id) {
		$model = $this->findModel($id);
		$addresses = Addresses::asArray();
		
		if($_POST['address_id']) {
			$address = Addresses::findOne(['id'=>$_POST['address_id']]);
			$address->organization_id = $model->id;
			$address->save();
			return $this->refresh();
		}
		
		return $this->render('organization-addresses', ['model'=>$model, 'addresses' => $addresses]);
	}
	
	public function actionOrganizationServices($id) {
		$model = $this->findModel($id);
		$service = new OrganizationServices;
		$service->organization_id = $model->id;
		
		if($service->load(Yii::$app->request->post())) {
			if($service->save()) {
				return $this->refresh();
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
		
		return $this->render('organization-services', ['model'=>$model, 'service'=>$service]);
	}
	
	public function actionEditService($id) {
		$service = OrganizationServices::findOne(['id'=>$id]);
		if($service->load(Yii::$app->request->post())) {
			if($service->save()) {
				return $this->redirect('/organization/admin/organization-services?id='.$service->organization_id);
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}
		return $this->render('organization-services-edit', ['model'=>$service]);
	}
	
	public function actionRemoveService($id) {
		OrganizationServices::findOne(['id'=>$id])->delete();
		return $this->redirect(Yii::$app->request->referrer);
	}
	
	protected function findModel($id)
    {
        if (($model = Organization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }/*
    
    public function actionTest() {
		Yii::$app->mailer->compose('@app/modules/organization/mail/view1-html') // a view rendering result becomes the message body here
		    ->setFrom('from@domain.com')
		    ->setTo('to@domain.com')
		    ->setSubject('Message subject')
		    ->send();
	}*/
    
}
