<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Modal;
	use yii\bootstrap\ActiveForm;
	use kartik\date\DatePicker;
?>
	
	
<?php	
	Modal::begin([
		'id' => 'm_form',
	    'header' => Yii::t('app', 'Добавление новости'),
	    'size' => 'modal-lg',
	    'options' => [
	    	'tabindex' => false,
	    ]/*,
	    'clientOptions'=> [
	    	'show' => true,
	    ]*/
	]);
	
	echo $this->render('_form_news_simple', ['action'=>'/news/admin/add-news', 'model'=>$model]);
	
	Modal::end();
?>
