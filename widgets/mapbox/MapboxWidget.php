<?php

namespace widgets\mapbox;

use yii\base\Widget;
use yii\helpers\Html;

class MapboxWidget extends Widget
{
    public $page;

    public function init()
    {
        parent::init();
    	\app\widgets\mapbox\assets\MapboxAsset::register($this->view);
        if ($this->page === null) {
            $this->page = 'default';
        }
    }

    // Добавлять динамично скрипт, в зависимости от указанного параметра - страницы
    public function run()
    {
        $this->view->registerJs($this->getJsFile("@widgets/mapbox/js/_main.js"), \yii\web\View::POS_END);
        switch ($this->page) {
            case 'report':
                $this->view->registerJs($this->getJsFile("@widgets/mapbox/js/report.js"), \yii\web\View::POS_END);
                break;
            case 'edit-moderator':
                $this->view->registerJs($this->getJsFile("@widgets/mapbox/js/edit-moderator.js"), \yii\web\View::POS_END);
                break;
            case 'map-index':
                $this->view->registerJs($this->getJsFile("@widgets/mapbox/js/map-index.js"), \yii\web\View::POS_END);
                break;
            case 'event-map':
                $this->view->registerJs($this->getJsFile("@widgets/mapbox/js/event-map.js"), \yii\web\View::POS_END);
                break;
            case 'statistics-map':
                $this->view->registerJs($this->getJsFile("@widgets/mapbox/js/statistics-map.js"), \yii\web\View::POS_END);
                break;

            default:
                // code...
                break;
        }
        // return Html::encode($this->message);
    }

    private function getJsFile($path) {

        $content = file_get_contents(\Yii::getAlias($path));
        if($content) {
            return $content;
        } else {
            return "console.log('cannot load file: $path')";
        }

    }
}
