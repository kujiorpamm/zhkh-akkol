var _vheight = $(window).height() - 177 - 120;
var hidden = [];
var alllayers = [];

$('#map').css( {
	height : _vheight + 'px'
});
map.resize();

map.on('load', function () {

	// Вытаскивает массив с сохраненными настройками
	cookies = Cookies.get('hidden');
	if(cookies != undefined) {
		hidden = JSON.parse(Cookies.get('hidden'));
	}

    geojsons.map(function(geojson) {
        map.loadImage(geojson.icon, function(error, image) {
            if (error) throw error;
            map.addImage(geojson.name + 'x', image);
			alllayers.push(geojson.name);
            map.addLayer({
                'id' : geojson.name,
                'type' : 'symbol',
                'source' : {
                    'type' : 'geojson',
                    'data' : geojson
                },
				'metadata' : {
                	'id' : geojson.id,
				},
                // "minzoom": 11,
                "layout": {
                    "icon-image": geojson.name + 'x',
					"icon-allow-overlap": true,
                    "icon-size": 1,
                    "text-field": "{title}",
                    "text-size": 9,
                    "text-offset": [0, 1],
                    "text-anchor": "top",
					"visibility" : hidden[geojson.id] == true ? "none" : "visible"
                }

            });

			if(hidden[geojson.id] == true) $('.item-toggle[data-id='+geojson.id+'] .visibility').addClass('closed');

            // POINTER EVENTS
            map.on('click', geojson.name, function (e) {
                new mapboxgl.Popup()
                    .setLngLat(e.lngLat)
                    .setHTML(e.features[0].properties.description)
                    .addTo(map);
            });
            map.on('mouseenter', geojson.name, function () {
                map.getCanvas().style.cursor = 'pointer';
            });
            map.on('mouseleave', geojson.name, function () {
                map.getCanvas().style.cursor = '';
            });

        });

    });
});

$(document).ready(function(e){

	$('.category .name').click(function(){
		$(this).parent().toggleClass('closed');

		window.setTimeout(function(){
			$('.menu').perfectScrollbar('update');
		}, 1000);


	});

	$('.map').attr('height', _vheight + 'px');
	$('.map').attr('style', 'height:' + _vheight + 'px');
	$('.menu').attr('style', 'max-height:' + (_vheight - 100) + 'px');


	init_elems();
	$('.menu').perfectScrollbar({suppressScrollX: true});

});


function init_elems() {

	$('.item-toggle').click(function() {
		$(this).toggleCollection();
	});
}

function hide_all_marks() {
	alllayers.map(function(item) {
		var clickedLayer = item;
		var layer_id = map.getLayer(item).metadata.id;
		map.setLayoutProperty(clickedLayer, 'visibility', 'none');
		hidden[layer_id] = true;
		$('.item-toggle[data-id='+layer_id+'] .visibility').addClass('closed');
		Cookies.set('hidden', JSON.stringify(hidden));
	});
}

function show_all_marks() {
	alllayers.map(function(item) {
		var clickedLayer = item;
		var layer_id = map.getLayer(item).metadata.id;
		map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
		hidden[layer_id] = false;
		$('.item-toggle[data-id='+layer_id+'] .visibility').removeClass('closed');
		Cookies.set('hidden', JSON.stringify(hidden));
	});
}
$.fn.toggleCollection = function() {

	var clickedLayer = 'layer-' + $(this).attr('data-id');
	var visibility = map.getLayoutProperty(clickedLayer, 'visibility');
	if (visibility === 'visible') {
		map.setLayoutProperty(clickedLayer, 'visibility', 'none');
		$(this).find('.visibility').addClass('closed');
		hidden[$(this).attr('data-id')] = true;
		Cookies.set('hidden', JSON.stringify(hidden));
	} else {
		map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
		$(this).find('.visibility').removeClass('closed');
		hidden[$(this).attr('data-id')] = false;
		Cookies.set('hidden', JSON.stringify(hidden));
	}

};
