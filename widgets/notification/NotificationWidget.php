<?php

namespace widgets\notification;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\Notifications;

class NotificationWidget extends Widget
{
    public $events;

    public function init()
    {
        parent::init();
    	// \app\widgets\mapbox\assets\MapboxAsset::register($this->view);

    }

    // Добавлять динамично скрипт, в зависимости от указанного параметра - страницы
    public function run()
    {

        $date = date('Y-m-d H:i:s');
        $models = Notifications::find()->where("datetime_from <= '$date'")->andWhere("'$date' <= datetime_to")->all();
        if($models) {
            return $this->render('default', [
                'models' => $models
            ]);
        } else {
            return;
        }
    }
}
